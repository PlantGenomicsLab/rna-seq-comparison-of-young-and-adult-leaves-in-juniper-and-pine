#!/bin/bash
#SBATCH --job-name=busco_pine
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o busco_%j.out
#SBATCH -e busco_%j.err


module load busco/5.0.0

busco -i Pine_final_annotations_no_contam_lvl0.faa -l /embryophyta_odb10 -o busco_pine -m Protein
