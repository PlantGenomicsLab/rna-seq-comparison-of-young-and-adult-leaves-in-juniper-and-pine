METRICS/TRANSCRIPTS                                    trinity_JF3A_index12.Trinity  
 == BASIC TRANSCRIPTS METRICS (calculated without reference genome and gene database) == 
Transcripts                                            84729                         

Transcripts > 500 bp                                   16336                         
Transcripts > 1000 bp                                  1287                          

Average length of assembled transcripts                428.552                       
Longest transcript                                     5096                          
Total length                                           36310760                      

Transcript N50                                         348                           
