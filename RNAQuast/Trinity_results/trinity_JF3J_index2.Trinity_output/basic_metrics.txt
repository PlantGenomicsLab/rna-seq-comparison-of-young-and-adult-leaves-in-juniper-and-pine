METRICS/TRANSCRIPTS                                    trinity_JF3J_index2.Trinity  
 == BASIC TRANSCRIPTS METRICS (calculated without reference genome and gene database) == 
Transcripts                                            110759                       

Transcripts > 500 bp                                   26496                        
Transcripts > 1000 bp                                  2818                         

Average length of assembled transcripts                452.666                      
Longest transcript                                     5150                         
Total length                                           50136847                     

Transcript N50                                         1015                         
