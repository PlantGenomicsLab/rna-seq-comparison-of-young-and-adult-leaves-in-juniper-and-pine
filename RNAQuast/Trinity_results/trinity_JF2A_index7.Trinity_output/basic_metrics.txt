METRICS/TRANSCRIPTS                                    trinity_JF2A_index7.Trinity  
 == BASIC TRANSCRIPTS METRICS (calculated without reference genome and gene database) == 
Transcripts                                            78364                        

Transcripts > 500 bp                                   28349                        
Transcripts > 1000 bp                                  5849                         

Average length of assembled transcripts                538.043                      
Longest transcript                                     5511                         
Total length                                           42163238                     

Transcript N50                                         2039                         
