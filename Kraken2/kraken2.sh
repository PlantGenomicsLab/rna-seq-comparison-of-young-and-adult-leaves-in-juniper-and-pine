#!/bin/bash
#SBATCH --job-name=kraken2
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 6
#SBATCH --partition=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --array=[0-11]%10
#SBATCH --mail-user=
#SBATCH -o kraken2_%j.out
#SBATCH -e kraken2_%j.err
#SBATCH --qos=general

module load kraken/2.0.8-beta
module load jellyfish/1.1.11

FILES1=($(ls -1 ../Quality_Control/*_R1_trimmed.fastq.gz))
FILES2=($(ls -1 ../Quality_Control/*_R2_trimmed.fastq.gz))
INT1=${FILES1[$SLURM_ARRAY_TASK_ID]}
INT2=${FILES2[$SLURM_ARRAY_TASK_ID]}
OUT1=$(echo $INT1 | sed 's/_R1_trimmed.fastq.gz/_kraken2_report.out/')
OUT2=$(echo $INT1 | sed 's/_R1_trimmed.fastq.gz/_kraken2.out/')


kraken2 --db kraken_db --report $OUT1 --fastq-input --paired --threads 3 $INT1 $INT2 > $OUT2 
