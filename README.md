# Comparing the RNA Sequence of Juvenile and Adult Leaves in Juniper and Pine

[TOC]
## Introduction
*Juniperus flaccida* (drooping juniper) and *Pinus cembroides* (pinyon pine) are conifers native to North America, spanning Mexico and the Southwestern region of the United States. Although from two different lineages, both species exhibit heteroblastic growth. Morphologically, their leaves undergo a change between the juvenile and adult life stage. *J. flaccida* leaves appear needle-like at youth and scale-like at maturity, whereas the *P. cembroides* will transition from needle-like leaves to brown scale-like leaves. The objective was to perform a comparative transcriptomic analysis to quantify and examine differential expression in juvenile and adult individuals from both species. RNA from twelve samples was sequenced on HiSeq 1500 (100bp PE) and analyzed with available software. Because there are no reference genomes for these species, they were assembled de novo from the RNA-Seq reads. Following assembly, the coding regions were identified and redundant transcripts were removed. Quality filtered reads were aligned to the reference transcriptomes (one for each species), counts were generated from the alignment files, and differential expression analysis was performed with DESeq2 via Kallisto. Up and down-regulated genes (padj<0.1) across both age classes (juvenile vs adult) were observed in each species and compared. 
<br>
### Comparisons of leaf arrangements in juvenile and adult branches of *J. flaccida* and *P.cembroides*
<br>

![]( https://gitlab.com/PlantGenomicsLab/rna-seq-comparison-of-young-and-adult-leaves-in-juniper-and-pine/-/raw/master/Figures/Figure1.jpg)
Scale like leaves of J. flaccida grow alternately along primary or secondary
branches (A-C). The juvenile stages of J. flaccida are distinguished by presenting a greater
internodal distance and larger leaves (A left and B) compared to adult stages (A right and C). In
P. cembroides (D-F), flattened juvenile needles grow helically arranged along a primary branch
(D right and E), whereas in adult stages, needles develop in secondary or tertiary dwarf shoots
(bundles) integrated by two or three needles (F) grouped by a basal sheath (s) in the axil of a
cataphyll (c)
<br>
### Anatomical comparisons in juvenile and adult leaves of *J. flaccida* and *P. cembroides* 
<br>

![]( https://gitlab.com/PlantGenomicsLab/rna-seq-comparison-of-young-and-adult-leaves-in-juniper-and-pine/-/raw/master/Figures/Figure2.jpg)
Juvenile scales in J. flaccida (A-C) have non-serrated margins (A-B), no
stomata on the abaxial surface (A) and two longitudinal clusters of stomata along almost the
entire leaf on the adaxial surface (B). In the transverse section (C) the presence of one dorsal
resin duct and abundant mesophyll is shown. In adult scales of J. flaccida (D-F) only a few
stomata are found on the sides of the base of the abaxial face (D), while the adaxial face (E) is
almost completely occupied by two clusters of stomata. The transverse section (F) includes two
alternate scales and the branch, showing that the conductive tissue (xylem and phloem) do not
penetrate leaves, which in turn, show abundant transfusion tissue. Juvenile leaves of P.
cembroides (G-I) have serrated margins (G-H), two lines of stomata on the abaxial side (G) and
four on the adaxial side (H). Pine juvenile leaves have two dorsal resin ducts under the
epidermis surrounded by one layer of hypodermis (I). Needles in adult trees (J-L) have non-
serrated margins (J-K) two rows of stomata on the abaxial side (J) and four on the adaxial face
(K). The resin ducts lie below the hypodermis and are surrounded by at least two layers of
hypodermic cells (L).
<br>
<br>
The original study can be accessed here: [2015 Proposal](https://gitlab.com/cynthiawebster/rna-seq-comparison-of-young-and-adult-leaves-in-juniper-and-pine/blob/master/CompleteUCMEXUS.es.en.pdf)
<br>

### Directory Layout
The data used in this project can be accessed in the Xanadu cluster at the following directory:`/labs/Wegrzyn/Juniper_Pine`
<br>
To view the files use the following command:
<br>
<pre style="color: silver; background: black;">-bash-4.2$ ls /labs/Wegrzyn/Juniper_Pine
Annotation Assembly Busco Clustering Coding_Region Counts DESeq2 Index Mapping Orthofinder Quality_Control RNAQuast </pre>
Sickle was already run on the raw data, so there was no need to re-do quality control. 
The summary of the data can be seen here: [QC_Analysis](https://gitlab.com/cynthiawebster/rna-seq-comparison-of-young-and-adult-leaves-in-juniper-and-pine/blob/master/Quality_Control/QC_Analysis.pdf)
Within the `Quality_Control` sub-directory, you will find all the trimmed fastq files of the different samples:
<pre style="color: silver; background: black;">
-bash-4.2$ ls Quality_Control/
all_JPJ_JF1A_index6_R1_trimmed.fastq      all_JPJ_JF2J_index5_R2_trimmed.fastq       all_JPJ_PC1A_index2_trimmed_single.fastq   all_JPJ_PC3A_index6_R1_trimmed.fastq
all_JPJ_JF1A_index6_R2_trimmed.fastq      all_JPJ_JF2J_index5_trimmed_single.fastq   all_JPJ_PC1J_index12_R1_trimmed.fastq      all_JPJ_PC3A_index6_R2_trimmed.fastq
all_JPJ_JF1A_index6_trimmed_single.fastq  all_JPJ_JF3A_index12_R1_trimmed.fastq      all_JPJ_PC1J_index12_R2_trimmed.fastq      all_JPJ_PC3A_index6_trimmed_single.fastq
all_JPJ_JF1J_index4_R1_trimmed.fastq      all_JPJ_JF3A_index12_R2_trimmed.fastq      all_JPJ_PC1J_index12_trimmed_single.fastq  all_JPJ_PC3J_index7_R1_trimmed.fastq
all_JPJ_JF1J_index4_R2_trimmed.fastq      all_JPJ_JF3A_index12_trimmed_single.fastq  all_JPJ_PC2A_index4_R1_trimmed.fastq       all_JPJ_PC3J_index7_R2_trimmed.fastq
all_JPJ_JF1J_index4_trimmed_single.fastq  all_JPJ_JF3J_index2_R1_trimmed.fastq       all_JPJ_PC2A_index4_R2_trimmed.fastq       all_JPJ_PC3J_index7_trimmed_single.fastq
all_JPJ_JF2A_index7_R1_trimmed.fastq      all_JPJ_JF3J_index2_R2_trimmed.fastq       all_JPJ_PC2A_index4_trimmed_single.fastq   multiple_sickle.sh
all_JPJ_JF2A_index7_R2_trimmed.fastq      all_JPJ_JF3J_index2_trimmed_single.fastq   all_JPJ_PC2J_index35_R1_trimmed.fastq      multiple_sickle.sh~
all_JPJ_JF2A_index7_trimmed_single.fastq  all_JPJ_PC1A_index2_R1_trimmed.fastq       all_JPJ_PC2J_index35_R2_trimmed.fastq      PC.R1.trimmed.fastq
all_JPJ_JF2J_index5_R1_trimmed.fastq      all_JPJ_PC1A_index2_R2_trimmed.fastq       all_JPJ_PC2J_index35_trimmed_single.fastq  PC.R2.trimmed.fastq</pre>

These are the associated names of all the samples given in the directory:
<br>

| Sample | Organism | Leaf Age | Species |  
| :------: | :------: |  :------: |  :------: |
| JPJ_JF1A | Juniper |Adult |*Juniperus flaccida* |
| JPJ_JF2A | Juniper |Adult |*Juniperus flaccida* | 
| JPJ_JF3A | Juniper |Adult |*Juniperus flaccida* |
| JPJ_JF1J | Juniper |Juvenile |*Juniperus flaccida* |
| JPJ_JF2J | Juniper |Juvenile |*Juniperus flaccida* |
| JPJ_JF3J | Juniper |Juvenile |*Juniperus flaccida* |
| JPJ_PC1A | Pine    |Adult    | *Pinus cembroides*  |
| JPJ_PC2A | Pine    |Adult    | *Pinus cembroides*  |
| JPJ_PC3A | Pine    |Adult    | *Pinus cembroides*  |
| JPJ_PC1J | Pine    |Juvenile | *Pinus cembroides*  |
| JPJ_PC2J | Pine    |Juvenile | *Pinus cembroides*  |
| JPJ_PC3J | Pine    |Juvenile | *Pinus cembroides*  |

<br>

## 1. Assembling the Transcriptome with Trinity
Because the genomes are not given, we had to construct them de novo using a transcript assembly software called Trinity. 
Trinity uses the trimmed fastq files from the Quality_Control directory to construct the transcriptome. 
Several scripts were used to run this software due to the immense RAM and time required; four libraries were run per job. In the `Assembly` sub-directory
you will find three shell scripts: `trinity.sh`, `trinity2.sh` and `trinity3.sh`. Here is a preview of what `trinity.sh` looks like:
<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=Trinity
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 36
#SBATCH --mem=128G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "trinity.sh"

module load trinity/2.6.6

Trinity --seqType fq \
        --left ../Quality_Control/all_JPJ_JF1A_index6_R1_trimmed.fastq \
        --right ../Quality_Control/all_JPJ_JF1A_index6_R2_trimmed.fastq \
        --min_contig_length 300 \
        --CPU 36 \
        --max_memory 100G \
        --output trinity_JF1A_index6 \
        --full_cleanup

Trinity --seqType fq \
        --left ../Quality_Control/all_JPJ_JF1J_index4_R1_trimmed.fastq \
        --right ../Quality_Control/all_JPJ_JF1J_index4_R2_trimmed.fastq \
        --min_contig_length 300 \
        --CPU 36 \
        --max_memory 100G \
        --output trinity_JF1J_index4 \
        --full_cleanup

Trinity --seqType fq \
        --left ../Quality_Control/all_JPJ_JF2A_index7_R1_trimmed.fastq \
        --right ../Quality_Control/all_JPJ_JF2A_index7_R2_trimmed.fastq \
        --min_contig_length 300 \
        --CPU 36 \
        --max_memory 100G \
        --output trinity_JF2A_index7 \
        --full_cleanup

Trinity --seqType fq \
        --left ../Quality_Control/all_JPJ_JF2J_index5_R1_trimmed.fastq \
        --right ../Quality_Control/all_JPJ_JF2J_index5_R2_trimmed.fastq \
        --min_contig_length 300 \
        --CPU 36 \
        --max_memory 100G \
        --output trinity_JF2J_index5 \
        --full_cleanup </pre>
        
Following the Trinity runs, the resulting file structure in the `Assembly` subdirectory was as follows:
<pre style="color: silver; background: black;">-bash-4.2$ ls 
trinity_JF1A_index6.Trinity.fasta
trinity_JF1A_index6.Trinity.fasta.gene_trans_map
trinity_JF1J_index4.Trinity.fasta
trinity_JF1J_index4.Trinity.fasta.gene_trans_map
trinity_JF2A_index7.Trinity.fasta
trinity_JF2A_index7.Trinity.fasta.gene_trans_map
trinity_JF2J_index5.Trinity.fasta
trinity_JF2J_index5.Trinity.fasta.gene_trans_map
trinity_JF3A_index12.Trinity.fasta
trinity_JF3A_index12.Trinity.fasta.gene_trans_map
trinity_JF3J_index2.Trinity.fasta
trinity_JF3J_index2.Trinity.fasta.gene_trans_map
trinity_PC1A_index2.Trinity.fasta
trinity_PC1A_index2.Trinity.fasta.gene_trans_map
trinity_PC1J_index12.Trinity.fasta
trinity_PC1J_index12.Trinity.fasta.gene_trans_map
trinity_PC2A_index4.Trinity.fasta
trinity_PC2A_index4.Trinity.fasta.gene_trans_map
trinity_PC2J_index35.Trinity.fasta
trinity_PC2J_index35.Trinity.fasta.gene_trans_map
trinity_PC3A_index6.Trinity.fasta
trinity_PC3A_index6.Trinity.fasta.gene_trans_map
trinity_PC3J_index7.Trinity.fasta
trinity_PC3J_index7.Trinity.fasta.gene_trans_map 
trinity.sh
trinity2.sh
trinity3.sh </pre>

### Run RNAQuast
To summarize the assembled transcripts, RNAQuast was run on each Trinity output.
The script, found in the `RNAQuast` sub-directory looks like this: 

<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=rnaQuast
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "rnaQUAST"

module load rnaQUAST/1.5.2
module load GeneMarkS-T/5.1

rnaQUAST.py --transcripts ../Assembly/trinity_JF1A_index6.Trinity.fasta ../Assembly/trinity_JF2A_index7.Trinity.fasta ../Assembly/trinity_JF3A_index12.Trinity.fasta ../Assembly/trinity_PC1A_index2.Trinity.fasta ../Assembly/trinity_PC2A_index4.Trinity.fasta ../Assembly/trinity_PC3A_index6.Trinity.fasta ../Assembly/trinity_JF1J_index4.Trinity.fasta ../Assembly/trinity_JF2J_index5.Trinity.fasta ../Assembly/trinity_JF3J_index2.Trinity.fasta ../Assembly/trinity_PC1J_index12.Trinity.fasta ../Assembly/trinity_PC2J_index35.Trinity.fasta ../Assembly/trinity_PC3J_index7.Trinity.fasta  \
        --gene_mark \
        --threads 8 \
        --output_dir results </pre>
The results can be viewed in the `RNAQuast` directory.
<br>

|             RNAQuast - Pine                                                           |PC1A   |PC2A   |PC3A   |PC1J|  PC2J|   PC3J|
|-------------------------------------------------------------------|-------|--------|------|----|-------|------|
|Transcripts                                                       |    68352   |63913| 65228|  77776|  103756| 82046|
|Transcripts > 500 bp                                                     |     25958|  19694|  23396|  27419|  35550|  27185|
|Transcripts > 1000 bp                                                  |       6022|   2937|   4272|   4911|   5859|   4751|
|Average length of assembled transcripts                                |553.586|       492.163|        525.753|        521.422|        513.244 |509.079|
|Longest transcript                                                   | 8114|   4912|   5823    |4997   |10452| 6502|
|Total length                                                           |37838696|      31455590|       34293793|       40554142|       53252094|       41767897|
|Transcript N50                                        |        1222|   433     |319|   471|    797     |1112|
<br>

|                 RNAQuast - Juniper                                                      |JF1A|        JF2A|   JF3A|   JF1J|   JF2J|   JF3J|
|-------------------------------------------------------------------|-----|-----|-------|-------|-------|-------|
|Transcripts                                                            |159283|        78364|  84729|  123905  |20308| 110759|
|Transcripts > 500 bp                                                |  66774|  28349|  16336|  35549|  3907|   26496|
|Transcripts > 1000 bp                                                | 17534|  5849|   1287|   4790|   311     |2818|
|Average length of assembled transcripts                        |       588.661 |538.043|       428.552|        478.675|        427.049 |452.666|
|Longest transcript                                                     |5532|  5511|   5096|   4637|   3064|   5150|
|Total length                                                       |   93763756|       42163238|       36310760|       59310167|       8672514 |50136847|
|Transcript N50                                         |516|   2039|   348     |366|   322|    1015|

## 2. Identifying Coding Region with TransDecoder
TransDecoder is a software that identifies coding regions in the transcript sequences
generated by Trinity. 

Before submitting any scripts, unique headers were created per each Trinity sample in the `Assembly` directory:
<pre style="color: silver; background: black;">
sed 's/>/>JF1J_/g' trinity_JF1J_index4.Trinity.fasta > JF1J_prefix.fasta
sed 's/>/>JF2J_/g' trinity_JF2J_index5.Trinity.fasta > JF2J_prefix.fasta
sed 's/>/>JF3J_/g' trinity_JF3J_index2.Trinity.fasta > JF3J_prefix.fasta
sed 's/>/>JF1A_/g' trinity_JF1A_index6.Trinity.fasta > JF1A_prefix.fasta
sed 's/>/>JF2A_/g' trinity_JF2A_index7.Trinity.fasta > JF2A_prefix.fasta
sed 's/>/>JF3A_/g' trinity_JF3A_index12.Trinity.fasta > JF3A_prefix.fasta
sed 's/>/>PC1A_/g' trinity_PC1A_index2.Trinity.fasta > PC1A_prefix.fasta
sed 's/>/>PC2A_/g' trinity_PC2A_index4.Trinity.fasta > PC2A_prefix.fasta
sed 's/>/>PC3A_/g' trinity_PC3A_index6.Trinity.fasta > PC3A_prefix.fasta
sed 's/>/>PC1J_/g' trinity_PC1J_index12.Trinity.fasta > PC1J_prefix.fasta
sed 's/>/>PC2J_/g' trinity_PC2J_index35.Trinity.fasta > PC2J_prefix.fasta
sed 's/>/>PC3J_/g' trinity_PC3J_index7.Trinity.fasta > PC3J_prefix.fasta </pre>

Next, these samples were combined into one file
<pre style="color: silver; background: black;">
cat JF1J_prefix.fasta JF2J_prefix.fasta JF3J_prefix.fasta >> JJ_combine.fasta
cat JF1A_prefix.fasta JF2A_prefix.fasta JF3A_prefix.fasta >> AJ_combine.fasta
cat PC1J_prefix.fasta PC2J_prefix.fasta PC3J_prefix.fasta >> JP_combine.fasta
cat PC1A_prefix.fasta PC2A_prefix.fasta PC3A_prefix.fasta >> AP_combine.fasta </pre>

Within the `Coding_Region` sub-directory you will find the scripts to run our job. The script was split into four parts for efficieny -- AJ, JJ, AP and JP.
Let's take a look at `transcoder_JJ.sh`
<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=transdecoder_JJ
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=150G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "transdecoder_JJ"

module load hmmer/3.2.1
module load TransDecoder/5.3.0

TransDecoder.LongOrfs -t ../Assembly/JJ_combine.fasta

hmmscan --cpu 16 --domtblout pfam.domtblout /isg/shared/databases/Pfam/Pfam-A.hmm JJ_combine.fasta.transdecoder_dir/longest_orfs.pep

TransDecoder.Predict -t ../Assembly/JJ_combine.fasta --no_refine_starts --retain_pfam_hits pfam.domtblout
 </pre>

TransDecoder works by first extracting the long open reading frames with `TransDecoder.LonOrfs`.
It then identifies ORFs with homology to known proteins with pfam searches, illustrated by `hmmscan`. Finally `TransDecoder.Predict`  predicts the likely coding regions 
in the sequences. 
<Br>
The job would not complete unless "--no_refine_starts" was included in the final line of the script. 
Overall, the following files were generated from this script:
<pre style="color: silver; background: black;">
JJ_combine.fasta.transdecoder_dir
JJ_combine.fasta.transdecoder_dir.__checkpoints
JJ_combine.fasta.transdecoder_dir.__checkpoints_longorfs
JJ_combine.fasta.transdecoder.bed
JJ_combine.fasta.transdecoder.cds
JJ_combine.fasta.transdecoder.gff3
JJ_combine.fasta.transdecoder.pep </pre>

When all four jobs were complete, the species .cds/.pep leaf samples were combined:
<pre style="color: silver; background: black;">
cat JJ_combine.fasta.transdecoder.cds AJ_combine.fasta.transdecoder.cds >> juniper_combine.transdecoder.cds
cat JJ_combine.fasta.transdecoder.pep AJ_combine.fasta.transdecoder.pep >> juniper_combine.transdecoder.pep
cat AP_combine.fasta.transdecoder.cds JP_combine.fasta.transdecoder.cds >> pine_combine.transdecoder.cds
cat AP_combine.fasta.transdecoder.pep JP_combine.fasta.transdecoder.pep >> pine_combine.transdecoder.pep </pre>

### Run RNAQuast
RNAQuast was run on the TransDecoder results for the separate age groups for juniper and pine (4) as well as for 
combined age groups (2) -- found in the `RNAQuast` directory. As shown below, the number of transcripts was reduced in each individual species transcriptome:
<br>

|RNAQuast - Pine |pine_combine.fasta.transdecoder.cds|
|-|-----------------------------------|
|Transcripts    |231054|
|Transcripts > 500 bp|  82265|
|Transcripts > 1000 bp  |11970|
|Average length of assembled transcripts|       511.606|
|Longest transcript|    6615|
|Total length|  118208592|
|Transcript N50|        759|
<br>

|RNAQuast - Juniper |juniper_combine.fasta.transdecoder.cds     |
|-|--------------------------------------|
|Transcripts |  229054|
|Transcripts > 500 bp | 78234|
|Transcripts > 1000 bp| 12796|
|Average length of assembled transcripts|       511.222|
|Longest transcript|    4881|
|Total length|  117097365|
|Transcript N50 |717|






## 3. Remove Contaminants with EnTAP at 80/80 Coverage
Individual PEP reference files (AJ, JJ, AP, JP) from TransDecoder were run through EnTAP to identify contaminants for removal. First, four Diamond databases were used: (1) complete.protein.faa.87.dmnd, (2) plant.protein.faa.97.dmnd , (3) uniprot_sprot.dmnd and (4) nr_protein.98.dmnd at 80/80 coverage. The job was split by each species’ age group in `Annotation/General_Contam_8080`. Below is the script for adult juniper:

<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=entap
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 30
#SBATCH --mem=80G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date
module load EnTAP/0.9.0-beta
module load diamond/0.9.19
module load GeneMarkS-T/5.1   

EnTAP --runP -i /labs/Wegrzyn/Juniper_Pine/Coding_Region/AJ_combine.fasta.transdecoder.pep -d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.87.dmnd  -d /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.97.dmnd -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd -d /isg/shared/databases/Diamond/ntnr/nr_protein.98.dmnd --tcoverage 80 --qcoverage 80 --taxon cupressaceae --contam bacteria --contam fungi --contam insecta  --threads 30 --out-dir Juniper_AJ 
</pre>
 
### Remove Contaminants 
Within the *final_results* directory per each reference was a file called: *final_annotations_contam_lvl0.faa*. This file is representative of all the contaminants that were flagged in the similarity search. To isolate the sequence headers per each reference the following command was used:

<pre style="color: silver; background: black;">
bash-4.2$ grep -h ">" final_annotations_contam_lvl0.faa > contam_header.txt 
bash-4.2$ sed -i 's/^.//' contam_header.txt
</pre>

The `contam_header.txt` was then transfered to the `Clustering` directory into the corresponding directory: `Contam_Removal_Juniper/Juniper_1`, and `Contam_Removal_Pine/Pine_1`. In each reference directory was a copied TransDecoder CDs file. To remove contaminants with `contam_header.txt` the following command was used (ex: AJ):

<pre style="color: silver; background: black;">
bash-4.2$ srun --partition=general --qos=general --mem=5g --pty bash
bash-4.2$ module load seqkit
# remove contaminated sequences with seqkit
bash-4.2$ seqkit grep -v -f contam.txt AJ_combine.fasta.transdecoder.cds > AJ.cds
# remove sequences less than 300 bp
bash-4.2$ seqkit seq -m 300 AJ.cds > AJ_300.cds
</pre>

Next, references were combined per each species with *cat*, producing:
(1) Juniper_300.cds
(2) Pine_300.cds

## 4. Clustering with VSEARCH
The next step was to remove redundant transcripts and shorten the transcriptome so that it is easier to work with. In the sub-directory `Clustering` you will find the scripts: `Juniper_Clustering.sh` and `Pine_Clustering.sh`. Here is `Pine_Clustering.sh`:
<pre style="color: silver; background: black;">

#!/bin/bash
#SBATCH --job-name=vsearch_Pine
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=100G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "vsearch_pine"

module load vsearch/2.4.3

vsearch --threads 8 --log LOGFile \
        --cluster_fast /labs/Wegrzyn/Juniper_Pine/Clustering/Contam_Removal_Pine/Pine_1/Pine_300.cds \
        --id 0.90 \
        --centroids centroids_Pine.fasta \
        --uc clusters_Pine.uc </pre>
        
The following was generated after job completition:
<pre style="color: silver; background: black;">-bash-4.2$ ls 
centroids_pine.fasta
clusters_pine.uc</pre>

### Identifying Duplicate FASTA Files: .pep vs .cds 
PEP transdecoder FASTA files (.pep = peptide sequences for the final candidate ORFs; all shorter candidates within longer ORFs are removed) were compared
to the original CDS files (.cds = the nucleotide coding sequence for all detected ORFs). A series of UNIX commands and seqtk was used to execute this task:

<pre style="color: silver; background: black;"> $ srun --partition=general --qos=general --mem=5G --pty bash <br>  $ module load seqtk/1.2</pre>

**Pine:**
<pre style="color: silver; background: black;">
$ grep -h ">" centroids_pine.fasta > centroid_pine_headers.txt
$ grep -h ">" centroids_pine_pep.fasta > centroid_pine_pep_headers.txt
$ cat centroid_pine_headers.txt centroid_pine_pep_headers.txt > pine_combine.txt
$ sed -i 's/^.//' pine_combine.txt
$ sort pine_combine.txt > pine_sorted_combine.txt
$ uniq -d pine_sorted_combine.txt > pine_duplicates.txt
$ seqtk subseq centroids_pine.fasta pine_duplicates.txt > pine_cds.fasta </pre>

**Juniper:**
<pre style="color: silver; background: black;">
$ grep -h ">" centroids_juniper.fasta > centroid_juniper_headers.txt
$ grep -h ">" centroids_juniper_pep.fasta > centroid_juniper_pep_headers.txt
$ cat centroid_juniper_headers.txt centroid_juniper_pep_headers.txt > juniper_combine.txt
$ sed -i 's/^.//' juniper_combine.txt
$ sort juniper_combine.txt > juniper_sorted_combine.txt
$ uniq -d juniper_sorted_combine.txt > juniper_duplicates.txt
$ seqtk subseq centroids_juniper.fasta juniper_duplicates.txt > juniper_cds.fasta </pre>

The ouput fasta files from seqtk were used for Kallisto. 

## 5. Remove Contaminants with EnTAP at 50/50 Coverage
In this run each combined species PEP file following initial contaminant removal and clustering was run against three Diamond databases: (1) complete.protein.faa.87.dmnd, (2) plant.protein.faa.97.dmnd , (3) uniprot_sprot.dmnd and at 50/50 coverage. The scripts can be found in `Annotation/General_Contam_5050`. Below is the script for juniper:

<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=entap
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 30
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date
module load EnTAP/0.9.0-beta
module load diamond/0.9.19
module load GeneMarkS-T/5.1   


EnTAP --runP -i /labs/Wegrzyn/Juniper_Pine/Clustering/Juniper.pep -d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.87.dmnd  -d /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.97.dmnd -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd --taxon cupressaceae --contam bacteria --contam fungi --contam insecta  --threads 30 --out-dir Juniper </pre>

### Remove Contaminants
Again, the headers from the contaminated sequences were isolated and brought to `Clustering/Contam_Removal_Juniper/Juniper_2` and `Clustering/Contam_Removal_Pine/Pine_2`. The same sequence was followed as before, producing: Pine.fasta and Juniper.fasta. CDs files were also isolated using the uncontaminated headers, producing 

 
### Run RNAQuast
At this point, we expected to see ~60,000 transcripts in our transcriptome. Pine.fasta and Juniper.fasta were run through RNAQuast: 
<br>

| RNAQuast - Pine | Pine.fasta |
|--|---|
|Transcripts                                        |           63741|
|Transcripts > 500 bp                                |          19344|
|Transcripts > 1000 bp                                |                     3178|
|Average length of assembled transcripts               |                491.217|
|Longest transcript                                     |                       6615|
|Total length                                            |              31310688|
|Transcript N50                                         |       1089|
<br>

| RNAQuast - Juniper | Juniper.fasta |
|---|-----|
|Transcripts      |                                                     69448|
|Transcripts > 500 bp   |                                               21167|
|Transcripts > 1000 bp   |                                                 4247|
|Average length of assembled transcripts |                              502.07|
|Longest transcript     |                                                  4881|
|Total length        |                                                 34867758|
|Transcript N50      |                                          492|

### Run BUSCO

BUSCO was run against the FAA file of each species. The scripts can be found in the `Busco` directory, below is the Juniper script: 

<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=busco_juniper
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o busco_%j.out
#SBATCH -e busco_%j.err


module load busco/4.0.2
module unload augustus
export PATH=/home/FCAM/cwebster/augustus/3.3.3/bin:/home/FCAM/cwebster/augustus/3.3.3/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=$HOME/augustus/3.3.3/config
module load python/3.8.1
module load hmmer/3.2.1
module load blast/2.7.1
module load R/3.6.3

busco -i Juniper_final_annotations_no_contam_lvl0.faa -l /isg/shared/databases/BUSCO/odb10/embryophyta_odb10 -o busco_juniper -m Protein </pre>

<br>

|BUSCO    | Juniper     | Pine     | 
|:------:|:-----:|:------:|
|Total Input Proteins|	69448|63741|
|Complete BUSCOs (C)|	431 (26.7%)|340 (21.0%)|
|Complete and single-copy BUSCOs (S)|	390 (24.4%)|291 (18.0%)|
|Complete and duplicated BUSCOs (D)|	41 (2.5%)|49 (3.0%)|
|Fragmented BUSCOs (F) |	430 (26.6%)|361 (22.4%)|
|Missing BUSCOs (M) |	751 (46.7%)|913 (55.6%)|
<br>

## 6. Indexing with Kallisto 
Kallisto is used to index a transcriptome. Each species’ transcriptome was indexed separately in the `Index` directory under a single script called `Index.sh` shown below:
<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=kallisto_index
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=20G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
##SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "kallisto_index"

module load kallisto/0.44.0

kallisto index -i juniper_index /labs/Wegrzyn/Juniper_Pine/Clustering/Juniper.fasta

kallisto index -i pine_index /labs/Wegrzyn/Juniper_Pine/Clustering/Pine.fasta

</pre>

The resulting file was: juniper_index and pine_index

## 7. Counts with Kallisto 
Following the indexing the read counts were extracted by kallisto processing paired-ends FASTQ files in the `Counts` directory.
This is an example of the juniper counts script, `Juniper.sh`:
<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=kallisto_counts_juniper
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname

module load kallisto/0.44.0

kallisto quant -i ../Index/juniper_index \
        -o JF1A \
        -t 8 --pseudobam \
    		/labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_JF1A_index6_R1_trimmed.fastq /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_JF1A_index6_R2_trimmed.fastq


kallisto quant -i ../Index/juniper_index \
        -o JF2A \
        -t 8 --pseudobam \
        /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_JF2A_index7_R1_trimmed.fastq /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_JF2A_index7_R2_trimmed.fastq

kallisto quant -i ../Index/juniper_index \
        -o JF3A \
        -t 8 --pseudobam \
        /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_JF3A_index12_R1_trimmed.fastq /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_JF3A_index12_R2_trimmed.fastq

kallisto quant -i ../Index/juniper_index \
        -o JF1J \
        -t 8 --pseudobam \
        /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_JF1J_index4_R1_trimmed.fastq /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_JF1J_index4_R2_trimmed.fastq

kallisto quant -i ../Index/juniper_index \
        -o JF2J \
        -t 8 --pseudobam \
        /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_JF2J_index5_R1_trimmed.fastq /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_JF2J_index5_R2_trimmed.fastq

kallisto quant -i ../Index/juniper_index \
        -o JF3J \
        -t 8 --pseudobam \
        /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_JF3J_index2_R1_trimmed.fastq /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_JF3J_index2_R2_trimmed.fastq

</pre>

The resulting output of the run was as follows:
<pre style="color: silver; background: black;">
-bash-4.2$ ls 
JF1A
JF2A
JF3A
JF1J
JF2J
JF3J </pre>
where each output contained four files: (1) *abundance.h5* (2) *abundance.tsv* (3) *pseudoalignments.bam* (4) *run_info.json*
The abundance.h5 is used in Sleuth DE analysis and abundance .tsv for DESeq2 so sample headers were added to the beginning of each file (ex: JF1A_abundance.h5)

### Percent Pseudoaligned
The run_info.json file tells you the total number of reads, the number of reads psuedoaligned, and percentage pseudoaligned. 

|Sample Names   |Percentage mapped|
|--------|--------|
|PC1A|  54.2%|
|PC2A|  52.6%|
|PC3A|  51.2%|
|PC1J|  53.3%|
|PC2J|  50.4%|
|PC3J|  47.9%|
|JF1A|  56.3%|
|JF2A|  24.8%|
|JF3A|  40.0%|
|JF1J|  45.8%|
|JF2J|  43.6%|
|JF3J|  43.5%|

It is generally acceptable to see mapping rates between 45-70% in a *de novo* assembly. Typically frame selection and contaminant removal will reduce the percetage mapped. 

## 8. Run Kraken2 and Identify Classified Reads (Do prior to Trinity to remove reads)
Kraken2 quantified the number of classified reads using the standard plus protozoa & fungi (​​PlusPF) database.

<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=kraken2
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 6
#SBATCH --partition=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --array=[0-11]%10
#SBATCH --mail-user=
#SBATCH -o kraken2_%j.out
#SBATCH -e kraken2_%j.err
#SBATCH --qos=general

module load kraken/2.0.8-beta
module load jellyfish/1.1.11

FILES1=($(ls -1 ../Quality_Control/*_R1_trimmed.fastq.gz))
FILES2=($(ls -1 ../Quality_Control/*_R2_trimmed.fastq.gz))
INT1=${FILES1[$SLURM_ARRAY_TASK_ID]}
INT2=${FILES2[$SLURM_ARRAY_TASK_ID]}
OUT1=$(echo $INT1 | sed 's/_R1_trimmed.fastq.gz/_kraken2_report.out/')
OUT2=$(echo $INT1 | sed 's/_R1_trimmed.fastq.gz/_kraken2.out/')


kraken2 --db kraken_db --report $OUT1 --fastq-input --paired --threads 3 $INT1 $INT2 > $OUT2 </pre>

Kraken2 output files can be visualized with Pavian via RStudio:

```R 
if (!require(remotes)) { install.packages("remotes") }
remotes::install_github("fbreitwieser/pavian")
pavian::runApp(port=5000)
```

## 9. Differential Expression Analysis with DESeq2
DESeq2 was used to generate DE genes for both pine and juniper separately (juvenile vs adult), with the log2 fold change being set to diverge from 1.5, and the padj being greater than 0.1. Kallisto "abundance.tsv" files were imported into RStudio using tximport. A tx2gene table
was generated first: 

<pre style="color: silver; background: black;">
**Pine:**
cd /labs/Wegrzyn/Juniper_Pine/Assembly
sed 's/TRINITY/PC1A_TRINITY/g' trinity_PC1A_index2.Trinity.fasta.gene_trans_map > PC1A_map.fasta 
sed 's/TRINITY/PC2A_TRINITY/g' trinity_PC2A_index4.Trinity.fasta.gene_trans_map > PC2A_map.fasta 
sed 's/TRINITY/PC3A_TRINITY/g' trinity_PC3A_index6.Trinity.fasta.gene_trans_map > PC3A_map.fasta 
sed 's/TRINITY/PC1J_TRINITY/g' trinity_PC1J_index12.Trinity.fasta.gene_trans_map > PC1J_map.fasta 
sed 's/TRINITY/PC2J_TRINITY/g' trinity_PC2J_index35.Trinity.fasta.gene_trans_map > PC2J_map.fasta 
sed 's/TRINITY/PC3J_TRINITY/g' trinity_PC3J_index7.Trinity.fasta.gene_trans_map > PC3J_map.fasta
cat PC1A_map.fasta PC2A_map.fasta PC3A_map.fasta PC1J_map.fasta PC2J_map.fasta PC3J_map.fasta >> pine_map
awk '{ print $2" " $1}' pine_map | sed '1 i\TXNAME\tGENEID' | sed 's/ /\t/g' > tx2gene_pine.tsv </pre>

<pre style="color: silver; background: black;">
**Juniper:**
cd /labs/Wegrzyn/Juniper_Pine/Assembly
sed 's/TRINITY/JF1A_TRINITY/g' trinity_JF1A_index6.Trinity.fasta.gene_trans_map > JF1A_map.fasta 
sed 's/TRINITY/JF2A_TRINITY/g' trinity_JF2A_index7.Trinity.fasta.gene_trans_map > JF2A_map.fasta 
sed 's/TRINITY/JF3A_TRINITY/g' trinity_JF3A_index12.Trinity.fasta.gene_trans_map > JF3A_map.fasta 
sed 's/TRINITY/JF1J_TRINITY/g' trinity_JF1J_index4.Trinity.fasta.gene_trans_map > JF1J_map.fasta 
sed 's/TRINITY/JF2J_TRINITY/g' trinity_JF2J_index5.Trinity.fasta.gene_trans_map > JF2J_map.fasta 
sed 's/TRINITY/JF3J_TRINITY/g' trinity_JF3J_index2.Trinity.fasta.gene_trans_map > JF3J_map.fasta
cat JF1A_map.fasta JF2A_map.fasta JF3A_map.fasta JF1J_map.fasta JF2J_map.fasta JF3J_map.fasta >> juniper_map
awk '{ print $2" " $1}' juniper_map | sed '1 i\TXNAME\tGENEID' | sed 's/ /\t/g' > tx2gene_juniper.tsv </pre>

This is the workflow of importing the TSV files for juniper, the script can be found in the `DESeq2` directory:
```R 
library("tximport")
library("readr")
library("DESeq2")
outputPrefix <- "juniper_deseq2"
setwd("C:/Juniper&Pine/Juniper_tsv")
filenames <- list.files(pattern="*abundance.tsv", full.names=TRUE)
tx2gene <- read.table("tx2gene_juniper.tsv", header=TRUE)
txi.kallisto <- tximport(filenames, type = "kallisto", tx2gene = tx2gene, ignoreTxVersion = TRUE)
sampleNames <- c("JF1A","JF1J","JF2A","JF2J","JF3A","JF3J")
colnames(txi.kallisto$counts) <- sampleNames
sampleCondition <- c("A","J","A","J","A","J")
sampleTable <- data.frame(Condition = sampleCondition, Sample = sampleNames)
ddstxi <- DESeqDataSetFromTximport(txi.kallisto, sampleTable, design = ~ Condition)
treatments <-  c("A","J")
colData(ddstxi)$Condition <- factor(colData(ddstxi)$Condition, levels = treatments)
```
The next step was to calculate differential expression using DESeq2 and produce plots:
```R
dds <- DESeq(ddstxi)
res <- results(dds)
transf <- res$padj
transfpad <- -log10(transf)
CFvolData <- data.frame("l2fc" = res$log2FoldChange, "log10padj" = transfpad)
CFvolData <- na.omit(CFvolData)
write.table(CFvolData, file = "CFvolcanodata.csv", sep = ",")
summary(res)
res= subset(res, padj<0.1)
res <- res[order(res$padj),]
resdata <- merge(as.data.frame(res),
                 as.data.frame(counts(dds,normalized =TRUE)),
                 by = 'row.names', sort = FALSE)
names(resdata)[1] <- 'gene'
write.csv(resdata, file = paste0(outputPrefix, "-results-with-normalized.csv"))
write.table(as.data.frame(counts(dds),normalized=T), 
            file = paste0(outputPrefix, "_normalized_counts.txt"), sep = '\t')
pos <- subset(res, log2FoldChange > 2)
neg <- subset(res, log2FoldChange < -2)
upreg2fold <- subset(pos, log2FoldChange < 4)
upreg4fold <- subset(pos, log2FoldChange > 4)
downreg2fold <- subset(neg, log2FoldChange > -4)
downreg4fold <- subset(neg, log2FoldChange < -4)
res2up <- merge(as.data.frame(upreg2fold),
                as.data.frame(counts(dds,normalized =TRUE)),
                by = 'row.names', sort = FALSE)
names(res2up)[1] <- 'gene'
write.csv(res2up, file = paste0(outputPrefix, "-upreg2fold.csv"))
res4up <- merge(as.data.frame(upreg4fold),
                as.data.frame(counts(dds,normalized =TRUE)),
                by = 'row.names', sort = FALSE)
names(res4up)[1] <- 'gene'
write.csv(res4up, file = paste0(outputPrefix, "-upreg4fold.csv"))
res2down <- merge(as.data.frame(downreg2fold),
                  as.data.frame(counts(dds,normalized =TRUE)),
                  by = 'row.names', sort = FALSE)
names(res2down)[1] <- 'gene'
write.csv(res2down, file = paste0(outputPrefix, "-downreg2fold.csv"))
res4down <- merge(as.data.frame(downreg4fold),
                  as.data.frame(counts(dds,normalized =TRUE)),
                  by = 'row.names', sort = FALSE)
names(res4down)[1] <- 'gene'
write.csv(res4down, file = paste0(outputPrefix, "-downreg4fold.csv"))
plotMA(dds, ylim=c(-8,8),main = "RNAseq Juniper")
dev.copy(png, paste0(outputPrefix, "-MAplot_initial_analysis.png"))
dev.off()
alpha <- 0.1
cols <- densCols(res$log2FoldChange, -log10(res$pvalue))
plot(res$log2FoldChange, -log10(res$padj), col=cols, panel.first=grid(),
     main="Volcano plot", xlab="Effect size: log2(fold-change)", ylab="-log10(adjusted p-value)",
     pch=20, cex=0.6)
abline(v=0)
abline(v=c(-1,1), col="brown")
abline(h=-log10(alpha), col="brown")
gn.selected <- abs(res$log2FoldChange) > 2.5 & res$padj < alpha 
text(res$log2FoldChange[gn.selected],
     -log10(res$padj)[gn.selected],
     lab=rownames(res)[gn.selected ], cex=0.4)
rld <- rlogTransformation(dds, blind=T)
vsd <- varianceStabilizingTransformation(dds, blind=T)
write.csv(as.data.frame(assay(rld)),file = paste0(outputPrefix, "-rlog-transformed-counts.txt"))
write.csv(as.data.frame(assay(vsd)),file = paste0(outputPrefix, "-vst-transformed-counts.txt"))
par(mai = ifelse(1:4 <= 2, par('mai'),0))
px <- counts(dds)[,1] / sizeFactors(dds)[1]
ord <- order(px)
ord <- ord[px[ord] < 150]
ord <- ord[seq(1,length(ord),length=50)]
last <- ord[length(ord)]
vstcol <- c('blue','black')
matplot(px[ord], cbind(assay(vsd)[,1], log2(px))[ord, ],type='l', lty = 1, col=vstcol, xlab = 'n', ylab = 'f(n)')
legend('bottomright',legend=c(expression('variance stabilizing transformation'), expression(log[2](n/s[1]))), fill=vstcol)
dev.copy(png,paste0(outputPrefix, "-variance_stabilizing.png"))
dev.off()
ddsClean <- replaceOutliersWithTrimmedMean(dds)
ddsClean <- DESeq(ddsClean)
tab <- table(initial = results(dds)$padj < 0.1,
             cleaned = results(ddsClean)$padj < 0.1)
addmargins(tab)
write.csv(as.data.frame(tab),file = paste0(outputPrefix, "-replaceoutliers.csv"))
resClean <- results(ddsClean)
resClean = subset(res, padj<0.1)
resClean <- resClean[order(resClean$padj),]
write.csv(as.data.frame(resClean),file = paste0(outputPrefix, "-replaceoutliers-results.csv"))

library("genefilter")
library("ggplot2")
library("grDevices")
rv <- rowVars(assay(rld))
select <- order(rv, decreasing=T)[seq_len(min(500,length(rv)))]
pc <- prcomp(t(assay(vsd)[select,]))
condition <- treatments
scores <- data.frame(pc$x, condition)

(pcaplot <- ggplot(scores, aes(x = PC1, y = PC2, col = (factor(condition))))
  + geom_point(size = 5)
  + ggtitle("Principal Components Juniper")
  + scale_colour_brewer(name = " ", palette = "Set1")
  + theme(
    plot.title = element_text(face = 'bold'),
    legend.position = c("bottom"),
    legend.key = element_rect(fill = 'NA'),
    legend.text = element_text(size = 10, face = "bold"),
    axis.text.y = element_text(colour = "Black"),
    axis.text.x = element_text(colour = "Black"),
    axis.title.x = element_text(face = "bold"),
    axis.title.y = element_text(face = 'bold'),
    panel.grid.major.x = element_blank(),
    panel.grid.major.y = element_blank(),
    panel.grid.minor.x = element_blank(),
    panel.grid.minor.y = element_blank(),
    panel.background = element_rect(color = 'black',fill = NA)))
ggsave(pcaplot,file=paste0(outputPrefix, "-ggplot2.png"))
head(assay(rld))
plot(log2(1+counts(dds,normalized=T)[,1:2]),col='black',pch=20,cex=0.3, main='Log2 transformed')
plot(assay(rld)[,1:2],col='#00000020',pch=20,cex=0.3, main = "rlog transformed")
plot(assay(rld)[,3:4],col='#00000020',pch=20,cex=0.3, main = "rlog transformed")
library("RColorBrewer")
library("gplots")
select <- order(rowMeans(counts(ddsClean,normalized=T)),decreasing=T)[1:30]
my_palette <- colorRampPalette(c("blue",'white','red'))(n=30)
heatmap(assay(vsd)[select,], col=my_palette,
          scale="row", key=T, keysize=1, symkey=T,
          density.info="none", trace="none",
          cexCol=0.6, labRow=F,
          main="Juniper")
dev.copy(png, paste0(outputPrefix, "-distanceHEATMAP.png"))
dev.off()
group <- factor(rep(1:9,each=6))
condition <- factor(rep(rep(c("N","C","S"),each=2),3))
d <- DataFrame(group, condition)[-c(17,18),]
as.data.frame(d)
m1 <- model.matrix(~ condition*group, d)
colnames(m1)
unname(m1)
sampleDists <- dist(t(assay(rld)))
suppressMessages(library("RColorBrewer"))
sampleDistMatrix <- as.matrix(sampleDists)
rownames(sampleDistMatrix) <- paste(colnames(rld), rld$type, sep="")
colnames(sampleDistMatrix) <- paste(colnames(rld), rld$type, sep="")
colors <- colorRampPalette( rev(brewer.pal(8, "Blues")) )(255)
heatmap(sampleDistMatrix,col=colors,margin = c(8,8))
dev.copy(png,paste0(outputPrefix, "-clustering.png"))
dev.off() 
```

![]( https://gitlab.com/PlantGenomicsLab/rna-seq-comparison-of-young-and-adult-leaves-in-juniper-and-pine/-/raw/master/Figures/pca_plot.PNG)

<br>
Above are the PCA plots derived from DESeq2.

## 10. Annotation of the DE Genes 
EnTAP was run against the DE genes of each reference (Juniper_DOWN, Juniper_UP, Pine_DOWN, Pine_UP) with three Diamond databases:(1) complete.protein.faa.87.dmnd, (2) plant.protein.faa.97.dmnd , (3) uniprot_sprot.dmnd at 50/50 coverage. The script for juniper (Juniper.sh) and pine (Pine.sh) can be found in `Annotation/DE`. Shown below is Juniper.sh:

<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=entap_juniper
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 30
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date
module load EnTAP/0.9.0-beta
module load diamond/0.9.19
module load GeneMarkS-T/5.1   

EnTAP --runP -i /labs/Wegrzyn/Juniper_Pine/DESeq2/Juniper/Juniper_DOWN.pep -d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.87.dmnd  -d /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.97.dmnd -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd --taxon cupressaceae --contam bacteria --contam fungi --contam insecta  --threads 30 --out-dir Juniper_DOWN


EnTAP --runP -i /labs/Wegrzyn/Juniper_Pine/DESeq2/Juniper/Juniper_UP.pep -d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.87.dmnd  -d /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.97.dmnd -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd --taxon cupressaceae --contam bacteria --contam fungi --contam insecta  --threads 30 --out-dir Juniper_UP </pre>


<br>
In the final_annotations_no_contam_lvl4.tsv file, the similarity search descriptions and EggNog predicted genes were identified and ordered by log2 fold change. This data can be found at: 
Plant Protein: https://docs.google.com/spreadsheets/d/1dFTZevEIlG1_D06xAY3ToI6FpCV1ScVv6r1Nd6qpJl8/edit?usp=sharing

<br>

## 11. Orthogroup Analysis with OrthoFinder
OrthoFinder was run against each individual age group/species’ FAA file from EnTAP. The DE genes were identified and “DE-” was put in front of each header for filtering purposes. Below is the script that can be found in `Orthofinder`:
<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=orthofinder
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o orthofinder-%j.output
#SBATCH -e orthofinder-%j.error


module load OrthoFinder/2.4.0
module load muscle
module load DLCpar/1.0
module load FastME
module load diamond/0.9.25
module load mcl
module load anaconda/4.4.0


orthofinder -f /labs/Wegrzyn/Juniper_Pine/Orthofinder -S diamond -t 16 </pre>

Below is a venn diagram illustrating orthogroups found in the whole proteome (A) and their abundance (B)

![](  https://gitlab.com/PlantGenomicsLab/rna-seq-comparison-of-young-and-adult-leaves-in-juniper-and-pine/-/raw/master/Figures/Figure_5.JPG)

## 12. Molecular GO Term Enrichment
Level 3 molecular function GO terms of differentially expressed genes were extracted from the EnTAP output, and effective lengths were derived from Kallisto’s output tsv. GOseq was then utilized in RStudio.

```R
#Juniper Down
gene.data <- integer(length=length(J_length$target_id))
names(gene.data) <- J_length$target_id
AbdUp <- down$V1
for (item in AbdUp) {
  i <- which(names(gene.data)==item)
  gene.data[i] <- 1
}
table(gene.data)
length(AbdUp) == table(gene.data)[2]
genes.list <- na.omit(gene.data)
feature.lengths <- J_length
cat.map <- juniper_go
GoCats <- cat.map[,c(1,3)]

genes.length.data <- filter(feature.lengths, target_id %in% names(genes.list))

genes.bias.data <- genes.length.data$eff_length
names(genes.bias.data) <- genes.length.data$target_id
pwf <- nullp(genes.list, bias.data = genes.bias.data, plot.fit = FALSE)

plotPWF(pwf = pwf, binsize = 50)
GO.wall <- goseq(pwf, gene2cat = GoCats, method = "Wallenius", use_genes_without_cat = FALSE)
library(dplyr)
sorted.GO <- arrange(GO.wall, GO.wall$over_represented_pvalue) 
```

![](https://gitlab.com/PlantGenomicsLab/rna-seq-comparison-of-young-and-adult-leaves-in-juniper-and-pine/-/raw/master/Figures/Figure_4.tif)

## 13. Gene Network Construction
GeneMANIA (plug-in version) was used to look at possible correlations amongst up and down regulated genes in each species with *Arabidopsis thaliana* as the assigned organism. Individual genes are color coded based on log2 fold change. Up-regulated genes are orange and down-regulated genes are purple. The deeper the color, the greater the significance. There were 20 resultant genes per each network, indicated by the color gray. All genes were organized by abundance of biological attributes.
 
<br>

The networks are defined as followed:
- **Shared domains**: Two genes are connected if they have the same protein domain. These linkages are established from domain databases like Pfam, InterPro and SMART.
- **Co-localization**: Two genes are connected if they are expressed in the same tissue or if they can be found in the same cellular location.
- **Co-expression**: Two genes are connected if they have similar expression levels across conditions in the study. This data is collected from the Gene Expression Omnibus (GEO) with an associated publication.
- **Predicted**: When the functional relationship between two genes is predicted via protein interactions. The mapping of known relationships from model organisms is by means of orthology. 

### Juniper (Juvenile vs Adult) 
*J. flaccida* saw unique photosynthetic functionality: 

![]( https://gitlab.com/PlantGenomicsLab/rna-seq-comparison-of-young-and-adult-leaves-in-juniper-and-pine/-/raw/master/Figures/Figure_7.png)
<br>
### Pine (Juvenile vs Adult)
*P. cembroides* had genes involved in cuticle and cell wall biosynthesis as well as those involved in development:
![](Figures/Screen_Shot_2021-06-28_at_12.29.37_PM.png)
   
          
