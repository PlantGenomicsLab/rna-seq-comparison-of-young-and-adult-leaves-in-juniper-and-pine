#!/bin/bash
#SBATCH --job-name=Trinity2
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 36
#SBATCH --mem=128G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "trinity2.sh"

module load trinity/2.6.6

Trinity --seqType fq \
        --left ../Quality_Control/all_JPJ_JF3A_index12_R1_trimmed.fastq \
        --right ../Quality_Control/all_JPJ_JF3A_index12_R2_trimmed.fastq \
        --min_contig_length 300 \
        --CPU 36 \
        --max_memory 100G \
        --output trinity_JF3A_index12 \
        --full_cleanup

Trinity --seqType fq \
        --left ../Quality_Control/all_JPJ_JF3J_index2_R1_trimmed.fastq \
        --right ../Quality_Control/all_JPJ_JF3J_index2_R2_trimmed.fastq \
        --min_contig_length 300 \
        --CPU 36 \
        --max_memory 100G \
        --output trinity_JF3J_index2 \
        --full_cleanup

Trinity --seqType fq \
        --left ../Quality_Control/all_JPJ_PC1A_index2_R1_trimmed.fastq \
        --right ../Quality_Control/all_JPJ_PC1A_index2_R2_trimmed.fastq \
        --min_contig_length 300 \
        --CPU 36 \
        --max_memory 100G \
        --output trinity_PC1A_index2 \
        --full_cleanup

Trinity --seqType fq \
        --left ../Quality_Control/all_JPJ_PC1J_index12_R1_trimmed.fastq \
        --right ../Quality_Control/all_JPJ_PC1J_index12_R2_trimmed.fastq \
        --min_contig_length 300 \
        --CPU 36 \
        --max_memory 100G \
        --output trinity_PC1J_index12 \
        --full_cleanup

