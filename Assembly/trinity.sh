#!/bin/bash
#SBATCH --job-name=Trinity
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 36
#SBATCH --mem=128G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "trinity.sh"

module load trinity/2.6.6

Trinity --seqType fq \
        --left ../Quality_Control/all_JPJ_JF1A_index6_R1_trimmed.fastq \
        --right ../Quality_Control/all_JPJ_JF1A_index6_R2_trimmed.fastq \
        --min_contig_length 300 \
        --CPU 36 \
        --max_memory 100G \
        --output trinity_JF1A_index6 \
        --full_cleanup

Trinity --seqType fq \
        --left ../Quality_Control/all_JPJ_JF1J_index4_R1_trimmed.fastq \
        --right ../Quality_Control/all_JPJ_JF1J_index4_R2_trimmed.fastq \
        --min_contig_length 300 \
        --CPU 36 \
        --max_memory 100G \
        --output trinity_JF1J_index4 \
        --full_cleanup

Trinity --seqType fq \
        --left ../Quality_Control/all_JPJ_JF2A_index7_R1_trimmed.fastq \
        --right ../Quality_Control/all_JPJ_JF2A_index7_R2_trimmed.fastq \
        --min_contig_length 300 \
        --CPU 36 \
        --max_memory 100G \
        --output trinity_JF2A_index7 \
        --full_cleanup

Trinity --seqType fq \
        --left ../Quality_Control/all_JPJ_JF2J_index5_R1_trimmed.fastq \
        --right ../Quality_Control/all_JPJ_JF2J_index5_R2_trimmed.fastq \
        --min_contig_length 300 \
        --CPU 36 \
        --max_memory 100G \
        --output trinity_JF2J_index5 \
        --full_cleanup

