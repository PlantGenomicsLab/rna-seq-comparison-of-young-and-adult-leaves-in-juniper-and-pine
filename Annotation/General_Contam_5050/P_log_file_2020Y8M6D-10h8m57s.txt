------------------------------------------------------
EnTAP Run Information - Execution
------------------------------------------------------
Current EnTAP Version: 0.9.0
Start time: Thu Aug  6 10:08:57 2020

Working directory has been set to: Pine

Execution Paths/Commands:

RSEM Directory: /isg/shared/apps/EnTAP/0.9.0-beta/libs/RSEM-1.3.0
GeneMarkS-T: perl /isg/shared/apps/EnTAP/0.9.0-beta/libs/gmst_linux_64/gmst.pl
DIAMOND: diamond
InterPro: interproscan.sh
EggNOG SQL Database: /isg/shared/apps/EnTAP/0.9.0-beta/databases/databases/eggnog.db
EggNOG DIAMOND Database: /isg/shared/apps/EnTAP/0.9.0-beta/databases/bin/eggnog_proteins.dmnd
EnTAP Database (binary): /isg/shared/apps/EnTAP/0.9.0-beta/databases/bin/entap_database.bin
EnTAP Database (SQL): /isg/shared/apps/EnTAP/0.9.0-beta//databases/entap_database.db
EnTAP Graphing Script: /isg/shared/apps/EnTAP/0.9.0-beta//src/entap_graphing.py

User Inputs:

contam: bacteria fungi insecta 
data-type: 0 
database: /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.87.dmnd /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.97.dmnd /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd 
e: 1.00e-05
fpkm: 0.50
input: /labs/Wegrzyn/Juniper_Pine/VSEARCH/Pine/pine_pep.pep
level: 0 3 4 
ontology: 0 
out-dir: Pine
output-format: 1 4 3 
protein: pfam 
qcoverage: 50.00
runP: null
state: +
taxon: pinaceae
tcoverage: 50.00
threads: 30

------------------------------------------------------
Transcriptome Statistics
------------------------------------------------------
Protein sequences found
Total sequences: 63839
Total length of transcriptome(bp): 52782201
Average sequence length(bp): 826.00
n50: 807
n90: 522
Longest sequence(bp): 16776 (PC3J_TRINITY_DN16233_c0_g1_i2.p1PC3J_TRINITY_DN16233_c0_g1~~PC3J_TRINITY_DN16233_c0_g1_i2.p1ORFtype:internallen:145(+),score=33.04,bZIP_1|PF00170.21|0.003,bZIP_1|PF00170.21|17,bZIP_1|PF00170.21|0.00057,bZIP_1|PF00170.21|19,DUF1043|PF06295.12|0.016,DUF1043|PF06295.12|0.03,DP|PF08781.10|0.037,DP|PF08781.10|0.028,CENP-F_leu_zip|PF10473.9|0.088,CENP-F_leu_zip|PF10473.9|0.014,SOGA|PF11365.8|0.13,SOGA|PF11365.8|3.9e+02,SOGA|PF11365.8|0.11,GAS|PF13851.6|0.071,GAS|PF13851.6|0.065,NPV_P10|PF05531.12|0.79,NPV_P10|PF05531.12|0.27,TACC_C|PF05010.14|18,TACC_C|PF05010.14|0.66,TACC_C|PF05010.14|0.55,DUF2664|PF10867.8|0.18,DUF2664|PF10867.8|0.19,TMF_TATA_bd|PF12325.8|0.32,TMF_TATA_bd|PF12325.8|0.048,DUF1376|PF07120.11|3,DUF1376|PF07120.11|2.5,Tropomyosin_1|PF12718.7|0.25,Tropomyosin_1|PF12718.7|0.084,Atg14|PF10186.9|0.13,Atg14|PF10186.9|0.074,DUF4201|PF13870.6|7.8,DUF4201|PF13870.6|0.0036,DUF4201|PF13870.6|16,DUF4156|PF13698.6|18,DUF4156|PF13698.6|0.16,DUF3450|PF11932.8|0.41,DUF3450|PF11932.8|0.12,LPP|PF04728.13|1.1,LPP|PF04728.13|0.56,HMMR_N|PF15905.5|0.025,HMMR_N|PF15905.5|4.3,Bacillus_HBL|PF05791.11|1.7,Bacillus_HBL|PF05791.11|0.65,Uso1_p115_C|PF04871.13|0.25,Uso1_p115_C|PF04871.13|0.5,Macoilin|PF09726.9|0.28,Macoilin|PF09726.9|0.16,BLOC1S3|PF15753.5|0.21,BLOC1S3|PF15753.5|7.1,DUF641|PF04859.12|0.55,DUF641|PF04859.12|45,DUF641|PF04859.12|1.2,USP8_interact|PF08941.10|0.41,USP8_interact|PF08941.10|0.58,UPF0242|PF06785.11|0.57,UPF0242|PF06785.11|0.39,ATG16|PF08614.11|1.8,ATG16|PF08614.11|0.31,LTXXQ|PF07813.12|0.77,LTXXQ|PF07813.12|0.8,JIP_LZII|PF16471.5|14,JIP_LZII|PF16471.5|6.7,ZapB|PF06005.12|45,ZapB|PF06005.12|0.048,ZapB|PF06005.12|78,Jnk-SapK_ap_N|PF09744.9|2.6,Phage_GP20|PF06810.11|1.4,Phage_GP20|PF06810.11|0.81,BLOC1_2|PF10046.9|2,BLOC1_2|PF10046.9|2.6,TSC22|PF01166.18|1.3,TSC22|PF01166.18|1.1e+02,TSC22|PF01166.18|0.43,TSC22|PF01166.18|1.3e+02,OmpH|PF03938.14|2.6,OmpH|PF03938.14|0.53,Dict-STAT-coil|PF09267.10|0.18,Dict-STAT-coil|PF09267.10|14,WD40_alt|PF14077.6|18,WD40_alt|PF14077.6|7.6,bZIP_2|PF07716.15|4.2,bZIP_2|PF07716.15|21,bZIP_2|PF07716.15|1.3,bZIP_2|PF07716.15|23,YabA|PF06156.13|7.1,YabA|PF06156.13|7.3e+02,AalphaY_MDB|PF04611.12|2.9,AalphaY_MDB|PF04611.12|2.9,STAT_alpha|PF01017.20|1.4,STAT_alpha|PF01017.20|1.1,BST2|PF16716.5|5.8,BST2|PF16716.5|9.7,BST2|PF16716.5|1.5,BST2|PF16716.5|8.8,Herpes_BLRF2|PF05812.12|10,Herpes_BLRF2|PF05812.12|5.8,Med21|PF11221.8|10,Med21|PF11221.8|3.7,Med21|PF11221.8|2.3,Med21|PF11221.8|3.5,CtIP_N|PF10482.9|0.023,CtIP_N|PF10482.9|3.5,DcpS_C|PF11969.8|7,DcpS_C|PF11969.8|12,TolA_bind_tri|PF16331.5|8.2,TolA_bind_tri|PF16331.5|67,Ribonuc_red_sm|PF00268.21|1.7,Ribonuc_red_sm|PF00268.21|7.8,JAKMIP_CC3|PF16034.5|4.2,JAKMIP_CC3|PF16034.5|34,JAKMIP_CC3|PF16034.5|1.9e+02,Nmad5|PF18757.1|0.98,Seryl_tRNA_N|PF02403.22|13,Seryl_tRNA_N|PF02403.22|4.1,Seryl_tRNA_N|PF02403.22|2.6,ERM|PF00769.19|1.8,DivIC|PF04977.15|2.3,DivIC|PF04977.15|41,DivIC|PF04977.15|1.5,DivIC|PF04977.15|30,DUF2321|PF10083.9|74,DUF2321|PF10083.9|0.46,Lebercilin|PF15619.6|11,Lebercilin|PF15619.6|17,DUF16|PF01519.16|2.9,DUF16|PF01519.16|6.3,S6OS1|PF15676.5|7.4,S6OS1|PF15676.5|0.55,DHR10|PF18595.1|3.8,DHR10|PF18595.1|5.4,Swi5|PF07061.11|0.85,Swi5|PF07061.11|81,Swi5|PF07061.11|0.72,Swi5|PF07061.11|1.1e+02,BRE1|PF08647.11|10,BRE1|PF08647.11|2.7,CASP_C|PF08172.12|0.3,CASP_C|PF08172.12|83,CASP_C|PF08172.12|0.27,CASP_C|PF08172.12|83,DUF2203|PF09969.9|7.2,DUF2203|PF09969.9|1.7,Mst1_SARAH|PF11629.8|4.3e+02,Mst1_SARAH|PF11629.8|2.7,Mst1_SARAH|PF11629.8|44,Mst1_SARAH|PF11629.8|2.7,DUF1484|PF07363.11|14,DUF1484|PF07363.11|19,Muted|PF14942.6|3.8,EMP24_GP25L|PF01105.24|8.4,EMP24_GP25L|PF01105.24|2.3,V_ATPase_I|PF01496.19|1.7,BicD|PF09730.9|1.7,Spc24|PF08286.11|34,Spc24|PF08286.11|33,MDH|PF02315.16|8.2,MDH|PF02315.16|6.8,Caldesmon|PF02029.15|0.094,Caldesmon|PF02029.15|1.5,APG6_N|PF17675.1|12,APG6_N|PF17675.1|7.5,IFT57|PF10498.9|4.1,IFT57|PF10498.9|3,DUF724|PF05266.14|5.2,DUF724|PF05266.14|2,HEPN_Swt1|PF18731.1|12,HEPN_Swt1|PF18731.1|15,Ax_dynein_light|PF10211.9|25,Ax_dynein_light|PF10211.9|8.4,betaPIX_CC|PF16523.5|39,betaPIX_CC|PF16523.5|3e+02,LXG|PF04740.12|2.5,LXG|PF04740.12|7.1,HAP1_N|PF04849.13|5.1,CCD48|PF15799.5|3.9,CCD48|PF15799.5|3.6,Fzo_mitofusin|PF04799.13|5.4,Fzo_mitofusin|PF04799.13|8.1,phiKZ_IP|PF12699.7|2.2,phiKZ_IP|PF12699.7|10,PRKG1_interact|PF15898.5|7.6,PRKG1_interact|PF15898.5|8.5,FUSC|PF04632.12|4.2,PP2C_C|PF07830.13|2.1,PP2C_C|PF07830.13|5.2,MCC-bdg_PDZ|PF10506.9|2.4,MCC-bdg_PDZ|PF10506.9|3.1e+02,MCC-bdg_PDZ|PF10506.9|2.5,MCC-bdg_PDZ|PF10506.9|2.9e+02,Xpo1|PF08389.12|5.9,Xpo1|PF08389.12|16,DMPK_coil|PF08826.10|2.5,DMPK_coil|PF08826.10|54,DMPK_coil|PF08826.10|4.5,DMPK_coil|PF08826.10|58,ADIP|PF11559.8|16,ADIP|PF11559.8|12,SlyX|PF04102.12|75,SlyX|PF04102.12|78,SlyX|PF04102.12|88,CENP-H|PF05837.12|5e+02,CENP-H|PF05837.12|15,zf-C4H2|PF10146.9|9.8,zf-C4H2|PF10146.9|6.5,SLATT_5|PF18160.1|8.6,SLATT_5|PF18160.1|13,CheZ|PF04344.13|12,CheZ|PF04344.13|5,Exonuc_VII_L|PF02601.15|8.4,Golgin_A5|PF09787.9|13,PDDEXK_6|PF04720.12|10,PDDEXK_6|PF04720.12|15,DUF3793|PF12672.7|28,DUF3793|PF12672.7|11,CENP-K|PF11802.8|9.3,Prominin|PF05478.11|14,Fmp27_WPPW|PF10359.9|32,Spectrin|PF00435.21|19,Spectrin|PF00435.21|5.1,PKcGMP_CC|PF16808.5|1.6,PKcGMP_CC|PF16808.5|4.4,Cob_adeno_trans|PF01923.18|2.5e+02,Csm1_N|PF18504.1|24,Csm1_N|PF18504.1|31,Csm1_N|PF18504.1|35,Csm1_N|PF18504.1|41,DUF4407|PF14362.6|14,CAML|PF14963.6|1.2e+02,TPR_MLP1_2|PF07926.12|11,TPR_MLP1_2|PF07926.12|11,KxDL|PF10241.9|4.7e+02,KxDL|PF10241.9|4.4,KxDL|PF10241.9|1.6e+02,KxDL|PF10241.9|5.3PC3J_TRINITY_DN16233_c0_g1_i2:1-432(+))
Shortest sequence(bp): 465 (PC1A_TRINITY_DN5937_c0_g1_i1.p2PC1A_TRINITY_DN5937_c0_g1~~PC1A_TRINITY_DN5937_c0_g1_i1.p2ORFtype:5prime_partiallen:100(+),score=3.98PC1A_TRINITY_DN5937_c0_g1_i1:2-301(+))
------------------------------------------------------
Similarity Search - DIAMOND - complete
------------------------------------------------------
Search results:
Pine/similarity_search/DIAMOND/blastp_pine_pep_final_complete.out
	Total alignments: 365991
	Total unselected results: 346087
		Written to: Pine/similarity_search/DIAMOND/processed//complete/unselected.tsv
	Total unique transcripts with an alignment: 19904
		Reference transcriptome sequences with an alignment (FASTA):
			Pine/similarity_search/DIAMOND/processed//complete/best_hits_lvl0
		Search results (TSV):
			Pine/similarity_search/DIAMOND/processed//complete/best_hits_lvl0
	Total unique transcripts without an alignment: 43935
		Reference transcriptome sequences without an alignment (FASTA):
			Pine/similarity_search/DIAMOND/processed//complete/no_hits.faa
	Total unique informative alignments: 10364
	Total unique uninformative alignments: 9540
	Total unique contaminants: 91(0.46%): 
		Transcriptome reference sequences labeled as a contaminant (FASTA):
			Pine/similarity_search/DIAMOND/processed//complete/best_hits_contam_lvl0
		Transcriptome reference sequences labeled as a contaminant (TSV):
			Pine/similarity_search/DIAMOND/processed//complete/best_hits_contam_lvl0
		Flagged contaminants (all % based on total contaminants):
			bacteria: 6(6.59%)
			insecta: 50(54.95%)
			fungi: 35(38.46%)
		Top 10 contaminants by species:
			1)metarhizium robertsii arsef 23: 18(19.78%)
			2)acromyrmex echinatior: 4(4.40%)
			3)serpula lacrymans var. lacrymans s7.9: 4(4.40%)
			4)pediculus humanus corporis: 4(4.40%)
			5)solenopsis invicta: 3(3.30%)
			6)vollenhovia emeryi: 3(3.30%)
			7)trachymyrmex zeteki: 3(3.30%)
			8)onthophagus taurus: 3(3.30%)
			9)drosophila grimshawi: 3(3.30%)
			10)diaphorina citri: 2(2.20%)
	Top 10 alignments by species:
			1)amborella trichopoda: 1736(8.72%)
			2)cucurbita pepo subsp. pepo: 1105(5.55%)
			3)olea europaea var. sylvestris: 654(3.29%)
			4)quercus suber: 630(3.17%)
			5)malus domestica: 502(2.52%)
			6)nelumbo nucifera: 481(2.42%)
			7)carica papaya: 467(2.35%)
			8)asparagus officinalis: 462(2.32%)
			9)nicotiana tabacum: 461(2.32%)
			10)hevea brasiliensis: 407(2.04%)

------------------------------------------------------
Similarity Search - DIAMOND - plant
------------------------------------------------------
Search results:
Pine/similarity_search/DIAMOND/blastp_pine_pep_final_plant.out
	Total alignments: 367160
	Total unselected results: 349243
		Written to: Pine/similarity_search/DIAMOND/processed//plant/unselected.tsv
	Total unique transcripts with an alignment: 17917
		Reference transcriptome sequences with an alignment (FASTA):
			Pine/similarity_search/DIAMOND/processed//plant/best_hits_lvl0
		Search results (TSV):
			Pine/similarity_search/DIAMOND/processed//plant/best_hits_lvl0
	Total unique transcripts without an alignment: 45922
		Reference transcriptome sequences without an alignment (FASTA):
			Pine/similarity_search/DIAMOND/processed//plant/no_hits.faa
	Total unique informative alignments: 12296
	Total unique uninformative alignments: 5621
	Total unique contaminants: 0(0.00%): 
		Transcriptome reference sequences labeled as a contaminant (FASTA):
			Pine/similarity_search/DIAMOND/processed//plant/best_hits_contam_lvl0
		Transcriptome reference sequences labeled as a contaminant (TSV):
			Pine/similarity_search/DIAMOND/processed//plant/best_hits_contam_lvl0
	Top 10 alignments by species:
			1)amborella trichopoda: 1448(8.08%)
			2)cucurbita pepo subsp. pepo: 922(5.15%)
			3)olea europaea var. sylvestris: 579(3.23%)
			4)camellia sinensis: 473(2.64%)
			5)elaeis guineensis: 450(2.51%)
			6)phoenix dactylifera: 442(2.47%)
			7)carica papaya: 435(2.43%)
			8)nelumbo nucifera: 395(2.20%)
			9)panicum hallii: 379(2.12%)
			10)hevea brasiliensis: 337(1.88%)

------------------------------------------------------
Similarity Search - DIAMOND - uniprot_sprot
------------------------------------------------------
Search results:
Pine/similarity_search/DIAMOND/blastp_pine_pep_final_uniprot_sprot.out
	Total alignments: 17873
	Total unselected results: 10957
		Written to: Pine/similarity_search/DIAMOND/processed//uniprot_sprot/unselected.tsv
	Total unique transcripts with an alignment: 6916
		Reference transcriptome sequences with an alignment (FASTA):
			Pine/similarity_search/DIAMOND/processed//uniprot_sprot/best_hits_lvl0
		Search results (TSV):
			Pine/similarity_search/DIAMOND/processed//uniprot_sprot/best_hits_lvl0
	Total unique transcripts without an alignment: 56923
		Reference transcriptome sequences without an alignment (FASTA):
			Pine/similarity_search/DIAMOND/processed//uniprot_sprot/no_hits.faa
	Total unique informative alignments: 6631
	Total unique uninformative alignments: 285
	Total unique contaminants: 230(3.33%): 
		Transcriptome reference sequences labeled as a contaminant (FASTA):
			Pine/similarity_search/DIAMOND/processed//uniprot_sprot/best_hits_contam_lvl0
		Transcriptome reference sequences labeled as a contaminant (TSV):
			Pine/similarity_search/DIAMOND/processed//uniprot_sprot/best_hits_contam_lvl0
		Flagged contaminants (all % based on total contaminants):
			fungi: 60(26.09%)
			insecta: 13(5.65%)
			bacteria: 157(68.26%)
		Top 10 contaminants by species:
			1)schizosaccharomyces pombe (strain 972 / atcc 24843): 33(14.35%)
			2)synechocystis sp. (strain pcc 6803 / kazusa): 26(11.30%)
			3)bacillus subtilis (strain 168): 18(7.83%)
			4)saccharomyces cerevisiae (strain atcc 204508 / s288c): 11(4.78%)
			5)escherichia coli (strain k12): 7(3.04%)
			6)mycobacterium tuberculosis (strain atcc 25618 / h37rv): 6(2.61%)
			7)halobacterium salinarum (strain atcc 700922 / jcm 11081 / nrc-1): 4(1.74%)
			8)escherichia coli o157:h7: 4(1.74%)
			9)thermosynechococcus elongatus (strain bp-1): 3(1.30%)
			10)yarrowia lipolytica (strain clib 122 / e 150): 3(1.30%)
	Top 10 alignments by species:
			1)arabidopsis thaliana: 3827(55.34%)
			2)oryza sativa subsp. japonica: 508(7.35%)
			3)nicotiana tabacum: 122(1.76%)
			4)homo sapiens: 97(1.40%)
			5)solanum lycopersicum: 87(1.26%)
			6)pisum sativum: 85(1.23%)
			7)oryza sativa subsp. indica: 85(1.23%)
			8)pinus koraiensis: 75(1.08%)
			9)zea mays: 74(1.07%)
			10)spinacia oleracea: 62(0.90%)

------------------------------------------------------
Compiled Similarity Search - DIAMOND - Best Overall
------------------------------------------------------

	Total unique transcripts with an alignment: 20754
		Reference transcriptome sequences with an alignment (FASTA):
			Pine/similarity_search/DIAMOND/overall_results/best_hits_lvl0
		Search results (TSV):
			Pine/similarity_search/DIAMOND/overall_results/best_hits_lvl0
	Total unique transcripts without an alignment: 43085
		Reference transcriptome sequences without an alignment (FASTA):
			Pine/similarity_search/DIAMOND/overall_results/no_hits.faa
	Total unique informative alignments: 13104
	Total unique uninformative alignments: 7650
	Total unique contaminants: 98(0.47%): 
		Transcriptome reference sequences labeled as a contaminant (FASTA):
			Pine/similarity_search/DIAMOND/overall_results/best_hits_contam_lvl0
		Transcriptome reference sequences labeled as a contaminant (TSV):
			Pine/similarity_search/DIAMOND/overall_results/best_hits_contam_lvl0
		Flagged contaminants (all % based on total contaminants):
			insecta: 45(45.92%)
			bacteria: 20(20.41%)
			fungi: 33(33.67%)
		Top 10 contaminants by species:
			1)metarhizium robertsii arsef 23: 15(15.31%)
			2)pediculus humanus corporis: 4(4.08%)
			3)drosophila grimshawi: 3(3.06%)
			4)vollenhovia emeryi: 3(3.06%)
			5)solenopsis invicta: 3(3.06%)
			6)onthophagus taurus: 3(3.06%)
			7)acromyrmex echinatior: 3(3.06%)
			8)serpula lacrymans var. lacrymans s7.9: 3(3.06%)
			9)trachymyrmex zeteki: 3(3.06%)
			10)phycomyces blakesleeanus nrrl 1555(-): 2(2.04%)
	Top 10 alignments by species:
			1)amborella trichopoda: 1257(6.06%)
			2)cucurbita pepo subsp. pepo: 1006(4.85%)
			3)arabidopsis thaliana: 658(3.17%)
			4)olea europaea var. sylvestris: 637(3.07%)
			5)quercus suber: 538(2.59%)
			6)malus domestica: 489(2.36%)
			7)carica papaya: 447(2.15%)
			8)phoenix dactylifera: 427(2.06%)
			9)nicotiana tabacum: 425(2.05%)
			10)asparagus officinalis: 386(1.86%)

------------------------------------------------------
Gene Family - Gene Ontology and Pathway - EggNOG
------------------------------------------------------
Statistics for overall Eggnog results: 
Total unique sequences with family assignment: 45326
Total unique sequences without family assignment: 18513
Top 10 Taxonomic Scopes Assigned:
	1)Viridiplantae: 41153(90.79%)
	2)Eukaryotes: 3270(7.21%)
	3)Ancestor: 443(0.98%)
	4)Arthropoda: 140(0.31%)
	5)Animals: 135(0.30%)
	6)Bacteria: 75(0.17%)
	7)Fungi: 56(0.12%)
	8)Mammals: 19(0.04%)
	9)Opisthokonts: 13(0.03%)
	10)Fishes: 12(0.03%)
Total unique sequences with at least one GO term: 45310
Total unique sequences without GO terms: 16
Total GO terms assigned: 2670015
Total molecular_function terms (lvl=0): 584072
Total unique molecular_function terms (lvl=0): 3007
Top 10 molecular_function terms assigned (lvl=0): 
	1)GO:0003674-molecular_function(L=0): 34258(5.87%)
	2)GO:0003824-catalytic activity(L=1): 24897(4.26%)
	3)GO:0005488-binding(L=1): 24786(4.24%)
	4)GO:0097159-organic cyclic compound binding(L=2): 19195(3.29%)
	5)GO:1901363-heterocyclic compound binding(L=2): 19180(3.28%)
	6)GO:0043167-ion binding(L=2): 18387(3.15%)
	7)GO:0036094-small molecule binding(L=2): 11799(2.02%)
	8)GO:1901265-nucleoside phosphate binding(L=3): 11625(1.99%)
	9)GO:0000166-nucleotide binding(L=3): 11625(1.99%)
	10)GO:0043168-anion binding(L=3): 10821(1.85%)
Total cellular_component terms (lvl=0): 586090
Total unique cellular_component terms (lvl=0): 1097
Top 10 cellular_component terms assigned (lvl=0): 
	1)GO:0005575-cellular_component(L=0): 31498(5.37%)
	2)GO:0044464-cell part(L=2): 29281(5.00%)
	3)GO:0005623-cell(L=1): 29281(5.00%)
	4)GO:0005622-intracellular(L=3): 26445(4.51%)
	5)GO:0044424-intracellular part(L=3): 26154(4.46%)
	6)GO:0043226-organelle(L=1): 23109(3.94%)
	7)GO:0043229-intracellular organelle(L=3): 23102(3.94%)
	8)GO:0043227-membrane-bounded organelle(L=2): 22417(3.82%)
	9)GO:0043231-intracellular membrane-bounded organelle(L=4): 22410(3.82%)
	10)GO:0005737-cytoplasm(L=4): 21734(3.71%)
Total overall terms (lvl=0): 2670015
Total unique overall terms (lvl=0): 11650
Top 10 overall terms assigned (lvl=0): 
	1)GO:0008150-biological_process(L=0): 35540(1.33%)
	2)GO:0003674-molecular_function(L=0): 34258(1.28%)
	3)GO:0005575-cellular_component(L=0): 31498(1.18%)
	4)GO:0008152-metabolic process(L=1): 29885(1.12%)
	5)GO:0005623-cell(L=1): 29281(1.10%)
	6)GO:0044464-cell part(L=2): 29281(1.10%)
	7)GO:0009987-cellular process(L=1): 28394(1.06%)
	8)GO:0005622-intracellular(L=3): 26445(0.99%)
	9)GO:0044424-intracellular part(L=3): 26154(0.98%)
	10)GO:0003824-catalytic activity(L=1): 24897(0.93%)
Total biological_process terms (lvl=0): 1499853
Total unique biological_process terms (lvl=0): 7546
Top 10 biological_process terms assigned (lvl=0): 
	1)GO:0008150-biological_process(L=0): 35540(2.37%)
	2)GO:0008152-metabolic process(L=1): 29885(1.99%)
	3)GO:0009987-cellular process(L=1): 28394(1.89%)
	4)GO:0071704-organic substance metabolic process(L=2): 24339(1.62%)
	5)GO:0044237-cellular metabolic process(L=2): 23410(1.56%)
	6)GO:0044238-primary metabolic process(L=2): 23277(1.55%)
	7)GO:0043170-macromolecule metabolic process(L=3): 17175(1.15%)
	8)GO:0044699-single-organism process(L=1): 16529(1.10%)
	9)GO:0044260-cellular macromolecule metabolic process(L=3): 15408(1.03%)
	10)GO:0044763-single-organism cellular process(L=2): 14054(0.94%)
Total molecular_function terms (lvl=3): 121750
Total unique molecular_function terms (lvl=3): 280
Top 10 molecular_function terms assigned (lvl=3): 
	1)GO:1901265-nucleoside phosphate binding(L=3): 11625(9.55%)
	2)GO:0000166-nucleotide binding(L=3): 11625(9.55%)
	3)GO:0043168-anion binding(L=3): 10821(8.89%)
	4)GO:0043169-cation binding(L=3): 10541(8.66%)
	5)GO:0003676-nucleic acid binding(L=3): 10086(8.28%)
	6)GO:0032553-ribonucleotide binding(L=3): 9525(7.82%)
	7)GO:0001882-nucleoside binding(L=3): 9425(7.74%)
	8)GO:0016772-transferase activity, transferring phosphorus-containing groups(L=3): 6592(5.41%)
	9)GO:0016817-hydrolase activity, acting on acid anhydrides(L=3): 4001(3.29%)
	10)GO:0016788-hydrolase activity, acting on ester bonds(L=3): 3073(2.52%)
Total cellular_component terms (lvl=3): 151045
Total unique cellular_component terms (lvl=3): 124
Top 10 cellular_component terms assigned (lvl=3): 
	1)GO:0005622-intracellular(L=3): 26445(17.51%)
	2)GO:0044424-intracellular part(L=3): 26154(17.32%)
	3)GO:0043229-intracellular organelle(L=3): 23102(15.29%)
	4)GO:0044446-intracellular organelle part(L=3): 12338(8.17%)
	5)GO:0071944-cell periphery(L=3): 9741(6.45%)
	6)GO:0005886-plasma membrane(L=3): 8421(5.58%)
	7)GO:0031224-intrinsic component of membrane(L=3): 7835(5.19%)
	8)GO:0031090-organelle membrane(L=3): 5279(3.49%)
	9)GO:0031975-envelope(L=3): 4165(2.76%)
	10)GO:0031967-organelle envelope(L=3): 4083(2.70%)
Total overall terms (lvl=3): 616576
Total unique overall terms (lvl=3): 829
Top 10 overall terms assigned (lvl=3): 
	1)GO:0005622-intracellular(L=3): 26445(4.29%)
	2)GO:0044424-intracellular part(L=3): 26154(4.24%)
	3)GO:0043229-intracellular organelle(L=3): 23102(3.75%)
	4)GO:0043170-macromolecule metabolic process(L=3): 17175(2.79%)
	5)GO:0044260-cellular macromolecule metabolic process(L=3): 15408(2.50%)
	6)GO:0044446-intracellular organelle part(L=3): 12338(2.00%)
	7)GO:1901360-organic cyclic compound metabolic process(L=3): 11917(1.93%)
	8)GO:0006725-cellular aromatic compound metabolic process(L=3): 11668(1.89%)
	9)GO:0000166-nucleotide binding(L=3): 11625(1.89%)
	10)GO:1901265-nucleoside phosphate binding(L=3): 11625(1.89%)
Total biological_process terms (lvl=3): 343781
Total unique biological_process terms (lvl=3): 425
Top 10 biological_process terms assigned (lvl=3): 
	1)GO:0043170-macromolecule metabolic process(L=3): 17175(5.00%)
	2)GO:0044260-cellular macromolecule metabolic process(L=3): 15408(4.48%)
	3)GO:1901360-organic cyclic compound metabolic process(L=3): 11917(3.47%)
	4)GO:0006725-cellular aromatic compound metabolic process(L=3): 11668(3.39%)
	5)GO:0034641-cellular nitrogen compound metabolic process(L=3): 11446(3.33%)
	6)GO:0046483-heterocycle metabolic process(L=3): 11367(3.31%)
	7)GO:1901576-organic substance biosynthetic process(L=3): 10736(3.12%)
	8)GO:0044249-cellular biosynthetic process(L=3): 10530(3.06%)
	9)GO:0006139-nucleobase-containing compound metabolic process(L=3): 10510(3.06%)
	10)GO:0019538-protein metabolic process(L=3): 10194(2.97%)
Total molecular_function terms (lvl=4): 115485
Total unique molecular_function terms (lvl=4): 557
Top 10 molecular_function terms assigned (lvl=4): 
	1)GO:0046872-metal ion binding(L=4): 10230(8.86%)
	2)GO:0032549-ribonucleoside binding(L=4): 9397(8.14%)
	3)GO:0017076-purine nucleotide binding(L=4): 9394(8.13%)
	4)GO:0032555-purine ribonucleotide binding(L=4): 9389(8.13%)
	5)GO:0001883-purine nucleoside binding(L=4): 9363(8.11%)
	6)GO:0035639-purine ribonucleoside triphosphate binding(L=4): 9143(7.92%)
	7)GO:0003677-DNA binding(L=4): 4384(3.80%)
	8)GO:0016301-kinase activity(L=4): 4349(3.77%)
	9)GO:0003723-RNA binding(L=4): 4288(3.71%)
	10)GO:0016818-hydrolase activity, acting on acid anhydrides, in phosphorus-containing anhydrides(L=4): 3955(3.42%)
Total cellular_component terms (lvl=4): 102686
Total unique cellular_component terms (lvl=4): 195
Top 10 cellular_component terms assigned (lvl=4): 
	1)GO:0043231-intracellular membrane-bounded organelle(L=4): 22410(21.82%)
	2)GO:0005737-cytoplasm(L=4): 21734(21.17%)
	3)GO:0044444-cytoplasmic part(L=4): 20238(19.71%)
	4)GO:0016021-integral component of membrane(L=4): 7655(7.45%)
	5)GO:0005829-cytosol(L=4): 6409(6.24%)
	6)GO:0005794-Golgi apparatus(L=4): 2808(2.73%)
	7)GO:0005618-cell wall(L=4): 2302(2.24%)
	8)GO:0030529-intracellular ribonucleoprotein complex(L=4): 2136(2.08%)
	9)GO:0005783-endoplasmic reticulum(L=4): 2031(1.98%)
	10)GO:0009579-thylakoid(L=4): 1983(1.93%)
Total overall terms (lvl=4): 550203
Total unique overall terms (lvl=4): 2052
Top 10 overall terms assigned (lvl=4): 
	1)GO:0043231-intracellular membrane-bounded organelle(L=4): 22410(4.07%)
	2)GO:0005737-cytoplasm(L=4): 21734(3.95%)
	3)GO:0044444-cytoplasmic part(L=4): 20238(3.68%)
	4)GO:0046872-metal ion binding(L=4): 10230(1.86%)
	5)GO:0032549-ribonucleoside binding(L=4): 9397(1.71%)
	6)GO:0017076-purine nucleotide binding(L=4): 9394(1.71%)
	7)GO:0032555-purine ribonucleotide binding(L=4): 9389(1.71%)
	8)GO:0001883-purine nucleoside binding(L=4): 9363(1.70%)
	9)GO:0035639-purine ribonucleoside triphosphate binding(L=4): 9143(1.66%)
	10)GO:0044267-cellular protein metabolic process(L=4): 8537(1.55%)
Total biological_process terms (lvl=4): 332032
Total unique biological_process terms (lvl=4): 1300
Top 10 biological_process terms assigned (lvl=4): 
	1)GO:0044267-cellular protein metabolic process(L=4): 8537(2.57%)
	2)GO:0006796-phosphate-containing compound metabolic process(L=4): 8531(2.57%)
	3)GO:0090304-nucleic acid metabolic process(L=4): 7912(2.38%)
	4)GO:0009059-macromolecule biosynthetic process(L=4): 6956(2.09%)
	5)GO:0034645-cellular macromolecule biosynthetic process(L=4): 6881(2.07%)
	6)GO:0043412-macromolecule modification(L=4): 6220(1.87%)
	7)GO:0036211-protein modification process(L=4): 5886(1.77%)
	8)GO:0010467-gene expression(L=4): 5783(1.74%)
	9)GO:0031323-regulation of cellular metabolic process(L=4): 4964(1.50%)
	10)GO:0080090-regulation of primary metabolic process(L=4): 4611(1.39%)
Total unique sequences with at least one pathway (KEGG) assignment: 13979
Total unique sequences without pathways (KEGG): 31347
Total pathways (KEGG) assigned: 49859
------------------------------------------------------
Final Annotation Statistics
------------------------------------------------------
Total Sequences: 63839
Similarity Search
	Total unique sequences with an alignment: 20754
	Total unique sequences without an alignment: 43085
Gene Families
	Total unique sequences with family assignment: 45326
	Total unique sequences without family assignment: 18513
	Total unique sequences with at least one GO term: 39615
	Total unique sequences with at least one pathway (KEGG) assignment: 13771
Totals
	Total unique sequences annotated (similarity search alignments only): 64
	Total unique sequences annotated (gene family assignment only): 24636
	Total unique sequences annotated (gene family and/or similarity search): 45390
	Total unique sequences unannotated (gene family and/or similarity search): 18449

EnTAP has completed! 
Total runtime (minutes): 242
