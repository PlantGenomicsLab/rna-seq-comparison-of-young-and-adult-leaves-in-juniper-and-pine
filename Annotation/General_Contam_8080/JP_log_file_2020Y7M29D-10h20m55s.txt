------------------------------------------------------
EnTAP Run Information - Execution
------------------------------------------------------
Current EnTAP Version: 0.9.0
Start time: Wed Jul 29 10:20:55 2020

Working directory has been set to: JP

Execution Paths/Commands:

RSEM Directory: /isg/shared/apps/EnTAP/0.9.0-beta/libs/RSEM-1.3.0
GeneMarkS-T: perl /isg/shared/apps/EnTAP/0.9.0-beta/libs/gmst_linux_64/gmst.pl
DIAMOND: diamond
InterPro: interproscan.sh
EggNOG SQL Database: /isg/shared/apps/EnTAP/0.9.0-beta/databases/databases/eggnog.db
EggNOG DIAMOND Database: /isg/shared/apps/EnTAP/0.9.0-beta/databases/bin/eggnog_proteins.dmnd
EnTAP Database (binary): /isg/shared/apps/EnTAP/0.9.0-beta/databases/bin/entap_database.bin
EnTAP Database (SQL): /isg/shared/apps/EnTAP/0.9.0-beta//databases/entap_database.db
EnTAP Graphing Script: /isg/shared/apps/EnTAP/0.9.0-beta//src/entap_graphing.py

User Inputs:

contam: bacteria fungi insecta 
data-type: 0 
database: /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.87.dmnd /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.97.dmnd /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd /isg/shared/databases/Diamond/ntnr/nr_protein.98.dmnd 
e: 1.00e-05
fpkm: 0.50
input: /labs/Wegrzyn/Juniper_Pine/Coding_Region/JP_combine.fasta.transdecoder.pep
level: 0 3 4 
ontology: 0 
out-dir: JP
output-format: 1 4 3 
protein: pfam 
qcoverage: 80.00
runP: null
state: +
taxon: cupressaceae
tcoverage: 80.00
threads: 30

------------------------------------------------------
Transcriptome Statistics
------------------------------------------------------
Protein sequences found
Total sequences: 130039
Total length of transcriptome(bp): 101599857
Average sequence length(bp): 781.00
n50: 762
n90: 519
Longest sequence(bp): 16776 (PC3J_TRINITY_DN16233_c0_g1_i2.p1PC3J_TRINITY_DN16233_c0_g1~~PC3J_TRINITY_DN16233_c0_g1_i2.p1ORFtype:internallen:145(+),score=33.04,bZIP_1|PF00170.21|0.003,bZIP_1|PF00170.21|17,bZIP_1|PF00170.21|0.00057,bZIP_1|PF00170.21|19,DUF1043|PF06295.12|0.016,DUF1043|PF06295.12|0.03,DP|PF08781.10|0.037,DP|PF08781.10|0.028,CENP-F_leu_zip|PF10473.9|0.088,CENP-F_leu_zip|PF10473.9|0.014,SOGA|PF11365.8|0.13,SOGA|PF11365.8|3.9e+02,SOGA|PF11365.8|0.11,GAS|PF13851.6|0.071,GAS|PF13851.6|0.065,NPV_P10|PF05531.12|0.79,NPV_P10|PF05531.12|0.27,TACC_C|PF05010.14|18,TACC_C|PF05010.14|0.66,TACC_C|PF05010.14|0.55,DUF2664|PF10867.8|0.18,DUF2664|PF10867.8|0.19,TMF_TATA_bd|PF12325.8|0.32,TMF_TATA_bd|PF12325.8|0.048,DUF1376|PF07120.11|3,DUF1376|PF07120.11|2.5,Tropomyosin_1|PF12718.7|0.25,Tropomyosin_1|PF12718.7|0.084,Atg14|PF10186.9|0.13,Atg14|PF10186.9|0.074,DUF4201|PF13870.6|7.8,DUF4201|PF13870.6|0.0036,DUF4201|PF13870.6|16,DUF4156|PF13698.6|18,DUF4156|PF13698.6|0.16,DUF3450|PF11932.8|0.41,DUF3450|PF11932.8|0.12,LPP|PF04728.13|1.1,LPP|PF04728.13|0.56,HMMR_N|PF15905.5|0.025,HMMR_N|PF15905.5|4.3,Bacillus_HBL|PF05791.11|1.7,Bacillus_HBL|PF05791.11|0.65,Uso1_p115_C|PF04871.13|0.25,Uso1_p115_C|PF04871.13|0.5,Macoilin|PF09726.9|0.28,Macoilin|PF09726.9|0.16,BLOC1S3|PF15753.5|0.21,BLOC1S3|PF15753.5|7.1,DUF641|PF04859.12|0.55,DUF641|PF04859.12|45,DUF641|PF04859.12|1.2,USP8_interact|PF08941.10|0.41,USP8_interact|PF08941.10|0.58,UPF0242|PF06785.11|0.57,UPF0242|PF06785.11|0.39,ATG16|PF08614.11|1.8,ATG16|PF08614.11|0.31,LTXXQ|PF07813.12|0.77,LTXXQ|PF07813.12|0.8,JIP_LZII|PF16471.5|14,JIP_LZII|PF16471.5|6.7,ZapB|PF06005.12|45,ZapB|PF06005.12|0.048,ZapB|PF06005.12|78,Jnk-SapK_ap_N|PF09744.9|2.6,Phage_GP20|PF06810.11|1.4,Phage_GP20|PF06810.11|0.81,BLOC1_2|PF10046.9|2,BLOC1_2|PF10046.9|2.6,TSC22|PF01166.18|1.3,TSC22|PF01166.18|1.1e+02,TSC22|PF01166.18|0.43,TSC22|PF01166.18|1.3e+02,OmpH|PF03938.14|2.6,OmpH|PF03938.14|0.53,Dict-STAT-coil|PF09267.10|0.18,Dict-STAT-coil|PF09267.10|14,WD40_alt|PF14077.6|18,WD40_alt|PF14077.6|7.6,bZIP_2|PF07716.15|4.2,bZIP_2|PF07716.15|21,bZIP_2|PF07716.15|1.3,bZIP_2|PF07716.15|23,YabA|PF06156.13|7.1,YabA|PF06156.13|7.3e+02,AalphaY_MDB|PF04611.12|2.9,AalphaY_MDB|PF04611.12|2.9,STAT_alpha|PF01017.20|1.4,STAT_alpha|PF01017.20|1.1,BST2|PF16716.5|5.8,BST2|PF16716.5|9.7,BST2|PF16716.5|1.5,BST2|PF16716.5|8.8,Herpes_BLRF2|PF05812.12|10,Herpes_BLRF2|PF05812.12|5.8,Med21|PF11221.8|10,Med21|PF11221.8|3.7,Med21|PF11221.8|2.3,Med21|PF11221.8|3.5,CtIP_N|PF10482.9|0.023,CtIP_N|PF10482.9|3.5,DcpS_C|PF11969.8|7,DcpS_C|PF11969.8|12,TolA_bind_tri|PF16331.5|8.2,TolA_bind_tri|PF16331.5|67,Ribonuc_red_sm|PF00268.21|1.7,Ribonuc_red_sm|PF00268.21|7.8,JAKMIP_CC3|PF16034.5|4.2,JAKMIP_CC3|PF16034.5|34,JAKMIP_CC3|PF16034.5|1.9e+02,Nmad5|PF18757.1|0.98,Seryl_tRNA_N|PF02403.22|13,Seryl_tRNA_N|PF02403.22|4.1,Seryl_tRNA_N|PF02403.22|2.6,ERM|PF00769.19|1.8,DivIC|PF04977.15|2.3,DivIC|PF04977.15|41,DivIC|PF04977.15|1.5,DivIC|PF04977.15|30,DUF2321|PF10083.9|74,DUF2321|PF10083.9|0.46,Lebercilin|PF15619.6|11,Lebercilin|PF15619.6|17,DUF16|PF01519.16|2.9,DUF16|PF01519.16|6.3,S6OS1|PF15676.5|7.4,S6OS1|PF15676.5|0.55,DHR10|PF18595.1|3.8,DHR10|PF18595.1|5.4,Swi5|PF07061.11|0.85,Swi5|PF07061.11|81,Swi5|PF07061.11|0.72,Swi5|PF07061.11|1.1e+02,BRE1|PF08647.11|10,BRE1|PF08647.11|2.7,CASP_C|PF08172.12|0.3,CASP_C|PF08172.12|83,CASP_C|PF08172.12|0.27,CASP_C|PF08172.12|83,DUF2203|PF09969.9|7.2,DUF2203|PF09969.9|1.7,Mst1_SARAH|PF11629.8|4.3e+02,Mst1_SARAH|PF11629.8|2.7,Mst1_SARAH|PF11629.8|44,Mst1_SARAH|PF11629.8|2.7,DUF1484|PF07363.11|14,DUF1484|PF07363.11|19,Muted|PF14942.6|3.8,EMP24_GP25L|PF01105.24|8.4,EMP24_GP25L|PF01105.24|2.3,V_ATPase_I|PF01496.19|1.7,BicD|PF09730.9|1.7,Spc24|PF08286.11|34,Spc24|PF08286.11|33,MDH|PF02315.16|8.2,MDH|PF02315.16|6.8,Caldesmon|PF02029.15|0.094,Caldesmon|PF02029.15|1.5,APG6_N|PF17675.1|12,APG6_N|PF17675.1|7.5,IFT57|PF10498.9|4.1,IFT57|PF10498.9|3,DUF724|PF05266.14|5.2,DUF724|PF05266.14|2,HEPN_Swt1|PF18731.1|12,HEPN_Swt1|PF18731.1|15,Ax_dynein_light|PF10211.9|25,Ax_dynein_light|PF10211.9|8.4,betaPIX_CC|PF16523.5|39,betaPIX_CC|PF16523.5|3e+02,LXG|PF04740.12|2.5,LXG|PF04740.12|7.1,HAP1_N|PF04849.13|5.1,CCD48|PF15799.5|3.9,CCD48|PF15799.5|3.6,Fzo_mitofusin|PF04799.13|5.4,Fzo_mitofusin|PF04799.13|8.1,phiKZ_IP|PF12699.7|2.2,phiKZ_IP|PF12699.7|10,PRKG1_interact|PF15898.5|7.6,PRKG1_interact|PF15898.5|8.5,FUSC|PF04632.12|4.2,PP2C_C|PF07830.13|2.1,PP2C_C|PF07830.13|5.2,MCC-bdg_PDZ|PF10506.9|2.4,MCC-bdg_PDZ|PF10506.9|3.1e+02,MCC-bdg_PDZ|PF10506.9|2.5,MCC-bdg_PDZ|PF10506.9|2.9e+02,Xpo1|PF08389.12|5.9,Xpo1|PF08389.12|16,DMPK_coil|PF08826.10|2.5,DMPK_coil|PF08826.10|54,DMPK_coil|PF08826.10|4.5,DMPK_coil|PF08826.10|58,ADIP|PF11559.8|16,ADIP|PF11559.8|12,SlyX|PF04102.12|75,SlyX|PF04102.12|78,SlyX|PF04102.12|88,CENP-H|PF05837.12|5e+02,CENP-H|PF05837.12|15,zf-C4H2|PF10146.9|9.8,zf-C4H2|PF10146.9|6.5,SLATT_5|PF18160.1|8.6,SLATT_5|PF18160.1|13,CheZ|PF04344.13|12,CheZ|PF04344.13|5,Exonuc_VII_L|PF02601.15|8.4,Golgin_A5|PF09787.9|13,PDDEXK_6|PF04720.12|10,PDDEXK_6|PF04720.12|15,DUF3793|PF12672.7|28,DUF3793|PF12672.7|11,CENP-K|PF11802.8|9.3,Prominin|PF05478.11|14,Fmp27_WPPW|PF10359.9|32,Spectrin|PF00435.21|19,Spectrin|PF00435.21|5.1,PKcGMP_CC|PF16808.5|1.6,PKcGMP_CC|PF16808.5|4.4,Cob_adeno_trans|PF01923.18|2.5e+02,Csm1_N|PF18504.1|24,Csm1_N|PF18504.1|31,Csm1_N|PF18504.1|35,Csm1_N|PF18504.1|41,DUF4407|PF14362.6|14,CAML|PF14963.6|1.2e+02,TPR_MLP1_2|PF07926.12|11,TPR_MLP1_2|PF07926.12|11,KxDL|PF10241.9|4.7e+02,KxDL|PF10241.9|4.4,KxDL|PF10241.9|1.6e+02,KxDL|PF10241.9|5.3PC3J_TRINITY_DN16233_c0_g1_i2:1-432(+))
Shortest sequence(bp): 462 (PC1J_TRINITY_DN1628_c0_g1_i1.p1PC1J_TRINITY_DN1628_c0_g1~~PC1J_TRINITY_DN1628_c0_g1_i1.p1ORFtype:3prime_partiallen:100(-),score=4.11PC1J_TRINITY_DN1628_c0_g1_i1:2-298(-))
------------------------------------------------------
Similarity Search - DIAMOND - complete
------------------------------------------------------
Search results:
JP/similarity_search/DIAMOND/blastp_JP_combine_final_complete.out
	Total alignments: 472911
	Total unselected results: 455752
		Written to: JP/similarity_search/DIAMOND/processed//complete/unselected.tsv
	Total unique transcripts with an alignment: 17159
		Reference transcriptome sequences with an alignment (FASTA):
			JP/similarity_search/DIAMOND/processed//complete/best_hits_lvl0
		Search results (TSV):
			JP/similarity_search/DIAMOND/processed//complete/best_hits_lvl0
	Total unique transcripts without an alignment: 112880
		Reference transcriptome sequences without an alignment (FASTA):
			JP/similarity_search/DIAMOND/processed//complete/no_hits.faa
	Total unique informative alignments: 9184
	Total unique uninformative alignments: 7975
	Total unique contaminants: 49(0.29%): 
		Transcriptome reference sequences labeled as a contaminant (FASTA):
			JP/similarity_search/DIAMOND/processed//complete/best_hits_contam_lvl0
		Transcriptome reference sequences labeled as a contaminant (TSV):
			JP/similarity_search/DIAMOND/processed//complete/best_hits_contam_lvl0
		Flagged contaminants (all % based on total contaminants):
			bacteria: 2(4.08%)
			insecta: 21(42.86%)
			fungi: 26(53.06%)
		Top 10 contaminants by species:
			1)metarhizium robertsii arsef 23: 14(28.57%)
			2)bipolaris oryzae atcc 44560: 7(14.29%)
			3)bombus impatiens: 4(8.16%)
			4)trachymyrmex septentrionalis: 2(4.08%)
			5)cryptotermes secundus: 2(4.08%)
			6)cladophialophora bantiana cbs 173.52: 2(4.08%)
			7)trachymyrmex cornetzi: 1(2.04%)
			8)culex quinquefasciatus: 1(2.04%)
			9)diachasma alloeum: 1(2.04%)
			10)vanderwaltozyma polyspora dsm 70294: 1(2.04%)
	Top 10 alignments by species:
			1)cucurbita pepo subsp. pepo: 1112(6.48%)
			2)amborella trichopoda: 1095(6.38%)
			3)olea europaea var. sylvestris: 571(3.33%)
			4)quercus suber: 558(3.25%)
			5)malus domestica: 481(2.80%)
			6)nicotiana tabacum: 403(2.35%)
			7)hevea brasiliensis: 397(2.31%)
			8)asparagus officinalis: 359(2.09%)
			9)arabidopsis lyrata subsp. lyrata: 349(2.03%)
			10)populus trichocarpa: 346(2.02%)

------------------------------------------------------
Similarity Search - DIAMOND - plant
------------------------------------------------------
Search results:
JP/similarity_search/DIAMOND/blastp_JP_combine_final_plant.out
	Total alignments: 457046
	Total unselected results: 441653
		Written to: JP/similarity_search/DIAMOND/processed//plant/unselected.tsv
	Total unique transcripts with an alignment: 15393
		Reference transcriptome sequences with an alignment (FASTA):
			JP/similarity_search/DIAMOND/processed//plant/best_hits_lvl0
		Search results (TSV):
			JP/similarity_search/DIAMOND/processed//plant/best_hits_lvl0
	Total unique transcripts without an alignment: 114646
		Reference transcriptome sequences without an alignment (FASTA):
			JP/similarity_search/DIAMOND/processed//plant/no_hits.faa
	Total unique informative alignments: 11019
	Total unique uninformative alignments: 4374
	Total unique contaminants: 0(0.00%): 
		Transcriptome reference sequences labeled as a contaminant (FASTA):
			JP/similarity_search/DIAMOND/processed//plant/best_hits_contam_lvl0
		Transcriptome reference sequences labeled as a contaminant (TSV):
			JP/similarity_search/DIAMOND/processed//plant/best_hits_contam_lvl0
	Top 10 alignments by species:
			1)cucurbita pepo subsp. pepo: 932(6.05%)
			2)amborella trichopoda: 887(5.76%)
			3)olea europaea var. sylvestris: 500(3.25%)
			4)camellia sinensis: 410(2.66%)
			5)phoenix dactylifera: 373(2.42%)
			6)hevea brasiliensis: 341(2.22%)
			7)panicum hallii: 337(2.19%)
			8)brassica napus: 333(2.16%)
			9)elaeis guineensis: 332(2.16%)
			10)carica papaya: 315(2.05%)

------------------------------------------------------
Similarity Search - DIAMOND - uniprot_sprot
------------------------------------------------------
Search results:
JP/similarity_search/DIAMOND/blastp_JP_combine_final_uniprot_sprot.out
	Total alignments: 18923
	Total unselected results: 14171
		Written to: JP/similarity_search/DIAMOND/processed//uniprot_sprot/unselected.tsv
	Total unique transcripts with an alignment: 4752
		Reference transcriptome sequences with an alignment (FASTA):
			JP/similarity_search/DIAMOND/processed//uniprot_sprot/best_hits_lvl0
		Search results (TSV):
			JP/similarity_search/DIAMOND/processed//uniprot_sprot/best_hits_lvl0
	Total unique transcripts without an alignment: 125287
		Reference transcriptome sequences without an alignment (FASTA):
			JP/similarity_search/DIAMOND/processed//uniprot_sprot/no_hits.faa
	Total unique informative alignments: 4650
	Total unique uninformative alignments: 102
	Total unique contaminants: 63(1.33%): 
		Transcriptome reference sequences labeled as a contaminant (FASTA):
			JP/similarity_search/DIAMOND/processed//uniprot_sprot/best_hits_contam_lvl0
		Transcriptome reference sequences labeled as a contaminant (TSV):
			JP/similarity_search/DIAMOND/processed//uniprot_sprot/best_hits_contam_lvl0
		Flagged contaminants (all % based on total contaminants):
			insecta: 3(4.76%)
			fungi: 14(22.22%)
			bacteria: 46(73.02%)
		Top 10 contaminants by species:
			1)synechocystis sp. (strain pcc 6803 / kazusa): 9(14.29%)
			2)streptomyces coelicolor (strain atcc baa-471 / a3(2) / m145): 5(7.94%)
			3)schizosaccharomyces pombe (strain 972 / atcc 24843): 4(6.35%)
			4)saccharomyces cerevisiae (strain atcc 204508 / s288c): 4(6.35%)
			5)escherichia coli o157:h7: 3(4.76%)
			6)bacillus subtilis (strain 168): 3(4.76%)
			7)aspergillus oryzae (strain atcc 42149 / rib 40): 2(3.17%)
			8)shewanella woodyi (strain atcc 51908 / ms32): 2(3.17%)
			9)ashbya gossypii (strain atcc 10895 / cbs 109.51 / fgsc 9923 / nrrl y-1056): 2(3.17%)
			10)aquifex pyrophilus: 2(3.17%)
	Top 10 alignments by species:
			1)arabidopsis thaliana: 2320(48.82%)
			2)oryza sativa subsp. japonica: 322(6.78%)
			3)pinus koraiensis: 125(2.63%)
			4)nicotiana tabacum: 101(2.13%)
			5)spinacia oleracea: 80(1.68%)
			6)glycine max: 80(1.68%)
			7)solanum lycopersicum: 69(1.45%)
			8)solanum tuberosum: 61(1.28%)
			9)picea mariana: 59(1.24%)
			10)zea mays: 54(1.14%)

------------------------------------------------------
Similarity Search - DIAMOND - nr_protein
------------------------------------------------------
Search results:
JP/similarity_search/DIAMOND/blastp_JP_combine_final_nr_protein.out
	Total alignments: 199612
	Total unselected results: 164207
		Written to: JP/similarity_search/DIAMOND/processed//nr_protein/unselected.tsv
	Total unique transcripts with an alignment: 35405
		Reference transcriptome sequences with an alignment (FASTA):
			JP/similarity_search/DIAMOND/processed//nr_protein/best_hits_lvl0
		Search results (TSV):
			JP/similarity_search/DIAMOND/processed//nr_protein/best_hits_lvl0
	Total unique transcripts without an alignment: 94634
		Reference transcriptome sequences without an alignment (FASTA):
			JP/similarity_search/DIAMOND/processed//nr_protein/no_hits.faa
	Total unique informative alignments: 16586
	Total unique uninformative alignments: 18819
	Total unique contaminants: 1426(4.03%): 
		Transcriptome reference sequences labeled as a contaminant (FASTA):
			JP/similarity_search/DIAMOND/processed//nr_protein/best_hits_contam_lvl0
		Transcriptome reference sequences labeled as a contaminant (TSV):
			JP/similarity_search/DIAMOND/processed//nr_protein/best_hits_contam_lvl0
		Flagged contaminants (all % based on total contaminants):
			fungi: 32(2.24%)
			insecta: 42(2.95%)
			bacteria: 1352(94.81%)
		Top 10 contaminants by species:
			1)acinetobacter baumannii: 602(42.22%)
			2)escherichia coli: 87(6.10%)
			3)soehngenia saccharolytica: 67(4.70%)
			4)klebsiella pneumoniae: 54(3.79%)
			5)enterobacter hormaechei: 38(2.66%)
			6)pelagivirga sediminicola: 18(1.26%)
			7)acetobacter malorum: 16(1.12%)
			8)clostridia bacterium k32: 16(1.12%)
			9)nitriliruptoraceae bacterium zyf776: 15(1.05%)
			10)enterobacter cloacae complex sp. 4dz1-17b1: 14(0.98%)
	Top 10 alignments by species:
			1)picea sitchensis: 5469(15.45%)
			2)vitis vinifera: 916(2.59%)
			3)zea mays: 829(2.34%)
			4)trifolium pratense: 791(2.23%)
			5)pinus taeda: 715(2.02%)
			6)acinetobacter baumannii: 602(1.70%)
			7)triticum turgidum subsp. durum: 558(1.58%)
			8)punica granatum: 544(1.54%)
			9)citrus sinensis: 517(1.46%)
			10)prunus yedoensis var. nudiflora: 465(1.31%)

------------------------------------------------------
Compiled Similarity Search - DIAMOND - Best Overall
------------------------------------------------------

	Total unique transcripts with an alignment: 35793
		Reference transcriptome sequences with an alignment (FASTA):
			JP/similarity_search/DIAMOND/overall_results/best_hits_lvl0
		Search results (TSV):
			JP/similarity_search/DIAMOND/overall_results/best_hits_lvl0
	Total unique transcripts without an alignment: 94246
		Reference transcriptome sequences without an alignment (FASTA):
			JP/similarity_search/DIAMOND/overall_results/no_hits.faa
	Total unique informative alignments: 17995
	Total unique uninformative alignments: 17798
	Total unique contaminants: 1111(3.10%): 
		Transcriptome reference sequences labeled as a contaminant (FASTA):
			JP/similarity_search/DIAMOND/overall_results/best_hits_contam_lvl0
		Transcriptome reference sequences labeled as a contaminant (TSV):
			JP/similarity_search/DIAMOND/overall_results/best_hits_contam_lvl0
		Flagged contaminants (all % based on total contaminants):
			fungi: 33(2.97%)
			insecta: 41(3.69%)
			bacteria: 1037(93.34%)
		Top 10 contaminants by species:
			1)acinetobacter baumannii: 373(33.57%)
			2)escherichia coli: 83(7.47%)
			3)klebsiella pneumoniae: 53(4.77%)
			4)soehngenia saccharolytica: 40(3.60%)
			5)enterobacter hormaechei: 24(2.16%)
			6)acetobacter malorum: 16(1.44%)
			7)nitriliruptoraceae bacterium zyf776: 15(1.35%)
			8)pelagivirga sediminicola: 14(1.26%)
			9)pseudomonas aeruginosa: 14(1.26%)
			10)enterobacter cloacae complex sp. 4dz1-17b1: 13(1.17%)
	Top 10 alignments by species:
			1)picea sitchensis: 4598(12.85%)
			2)zea mays: 821(2.29%)
			3)cucurbita pepo subsp. pepo: 786(2.20%)
			4)vitis vinifera: 785(2.19%)
			5)trifolium pratense: 747(2.09%)
			6)pinus taeda: 727(2.03%)
			7)triticum turgidum subsp. durum: 527(1.47%)
			8)citrus sinensis: 473(1.32%)
			9)prunus yedoensis var. nudiflora: 439(1.23%)
			10)punica granatum: 429(1.20%)

------------------------------------------------------
Gene Family - Gene Ontology and Pathway - EggNOG
------------------------------------------------------
Statistics for overall Eggnog results: 
Total unique sequences with family assignment: 104712
Total unique sequences without family assignment: 25327
Top 10 Taxonomic Scopes Assigned:
	1)Viridiplantae: 96370(92.03%)
	2)Eukaryotes: 6670(6.37%)
	3)Ancestor: 891(0.85%)
	4)Arthropoda: 275(0.26%)
	5)Animals: 216(0.21%)
	6)Bacteria: 156(0.15%)
	7)Fungi: 55(0.05%)
	8)Opisthokonts: 29(0.03%)
	9)Fishes: 21(0.02%)
	10)Mammals: 20(0.02%)
Total unique sequences with at least one GO term: 104683
Total unique sequences without GO terms: 29
Total GO terms assigned: 6580540
Total molecular_function terms (lvl=0): 1402515
Total unique molecular_function terms (lvl=0): 3044
Top 10 molecular_function terms assigned (lvl=0): 
	1)GO:0003674-molecular_function(L=0): 81956(5.84%)
	2)GO:0003824-catalytic activity(L=1): 60230(4.29%)
	3)GO:0005488-binding(L=1): 59551(4.25%)
	4)GO:0097159-organic cyclic compound binding(L=2): 45684(3.26%)
	5)GO:1901363-heterocyclic compound binding(L=2): 45638(3.25%)
	6)GO:0043167-ion binding(L=2): 44395(3.17%)
	7)GO:0036094-small molecule binding(L=2): 29004(2.07%)
	8)GO:0000166-nucleotide binding(L=3): 28568(2.04%)
	9)GO:1901265-nucleoside phosphate binding(L=3): 28568(2.04%)
	10)GO:0043168-anion binding(L=3): 26396(1.88%)
Total cellular_component terms (lvl=0): 1483437
Total unique cellular_component terms (lvl=0): 1157
Top 10 cellular_component terms assigned (lvl=0): 
	1)GO:0005575-cellular_component(L=0): 76480(5.16%)
	2)GO:0005623-cell(L=1): 71589(4.83%)
	3)GO:0044464-cell part(L=2): 71589(4.83%)
	4)GO:0005622-intracellular(L=3): 64919(4.38%)
	5)GO:0044424-intracellular part(L=3): 64246(4.33%)
	6)GO:0043226-organelle(L=1): 56876(3.83%)
	7)GO:0043229-intracellular organelle(L=3): 56863(3.83%)
	8)GO:0043227-membrane-bounded organelle(L=2): 55417(3.74%)
	9)GO:0043231-intracellular membrane-bounded organelle(L=4): 55404(3.73%)
	10)GO:0005737-cytoplasm(L=4): 54892(3.70%)
Total overall terms (lvl=0): 6580540
Total unique overall terms (lvl=0): 12151
Top 10 overall terms assigned (lvl=0): 
	1)GO:0008150-biological_process(L=0): 84639(1.29%)
	2)GO:0003674-molecular_function(L=0): 81956(1.25%)
	3)GO:0005575-cellular_component(L=0): 76480(1.16%)
	4)GO:0008152-metabolic process(L=1): 71732(1.09%)
	5)GO:0005623-cell(L=1): 71589(1.09%)
	6)GO:0044464-cell part(L=2): 71589(1.09%)
	7)GO:0009987-cellular process(L=1): 68384(1.04%)
	8)GO:0005622-intracellular(L=3): 64919(0.99%)
	9)GO:0044424-intracellular part(L=3): 64246(0.98%)
	10)GO:0003824-catalytic activity(L=1): 60230(0.92%)
Total biological_process terms (lvl=0): 3694588
Total unique biological_process terms (lvl=0): 7950
Top 10 biological_process terms assigned (lvl=0): 
	1)GO:0008150-biological_process(L=0): 84639(2.29%)
	2)GO:0008152-metabolic process(L=1): 71732(1.94%)
	3)GO:0009987-cellular process(L=1): 68384(1.85%)
	4)GO:0071704-organic substance metabolic process(L=2): 58550(1.58%)
	5)GO:0044237-cellular metabolic process(L=2): 56854(1.54%)
	6)GO:0044238-primary metabolic process(L=2): 55941(1.51%)
	7)GO:0043170-macromolecule metabolic process(L=3): 40318(1.09%)
	8)GO:0044699-single-organism process(L=1): 39991(1.08%)
	9)GO:0044260-cellular macromolecule metabolic process(L=3): 36182(0.98%)
	10)GO:0050896-response to stimulus(L=1): 34082(0.92%)
Total molecular_function terms (lvl=3): 294243
Total unique molecular_function terms (lvl=3): 285
Top 10 molecular_function terms assigned (lvl=3): 
	1)GO:0000166-nucleotide binding(L=3): 28568(9.71%)
	2)GO:1901265-nucleoside phosphate binding(L=3): 28568(9.71%)
	3)GO:0043168-anion binding(L=3): 26396(8.97%)
	4)GO:0043169-cation binding(L=3): 25370(8.62%)
	5)GO:0032553-ribonucleotide binding(L=3): 23133(7.86%)
	6)GO:0003676-nucleic acid binding(L=3): 22962(7.80%)
	7)GO:0001882-nucleoside binding(L=3): 22830(7.76%)
	8)GO:0016772-transferase activity, transferring phosphorus-containing groups(L=3): 14945(5.08%)
	9)GO:0016817-hydrolase activity, acting on acid anhydrides(L=3): 9922(3.37%)
	10)GO:0016788-hydrolase activity, acting on ester bonds(L=3): 6670(2.27%)
Total cellular_component terms (lvl=3): 380104
Total unique cellular_component terms (lvl=3): 131
Top 10 cellular_component terms assigned (lvl=3): 
	1)GO:0005622-intracellular(L=3): 64919(17.08%)
	2)GO:0044424-intracellular part(L=3): 64246(16.90%)
	3)GO:0043229-intracellular organelle(L=3): 56863(14.96%)
	4)GO:0044446-intracellular organelle part(L=3): 31780(8.36%)
	5)GO:0071944-cell periphery(L=3): 25047(6.59%)
	6)GO:0005886-plasma membrane(L=3): 21663(5.70%)
	7)GO:0031224-intrinsic component of membrane(L=3): 18710(4.92%)
	8)GO:0031090-organelle membrane(L=3): 13945(3.67%)
	9)GO:0031975-envelope(L=3): 11116(2.92%)
	10)GO:0031967-organelle envelope(L=3): 10979(2.89%)
Total overall terms (lvl=3): 1522923
Total unique overall terms (lvl=3): 849
Top 10 overall terms assigned (lvl=3): 
	1)GO:0005622-intracellular(L=3): 64919(4.26%)
	2)GO:0044424-intracellular part(L=3): 64246(4.22%)
	3)GO:0043229-intracellular organelle(L=3): 56863(3.73%)
	4)GO:0043170-macromolecule metabolic process(L=3): 40318(2.65%)
	5)GO:0044260-cellular macromolecule metabolic process(L=3): 36182(2.38%)
	6)GO:0044446-intracellular organelle part(L=3): 31780(2.09%)
	7)GO:1901265-nucleoside phosphate binding(L=3): 28568(1.88%)
	8)GO:0000166-nucleotide binding(L=3): 28568(1.88%)
	9)GO:1901360-organic cyclic compound metabolic process(L=3): 28091(1.84%)
	10)GO:0006725-cellular aromatic compound metabolic process(L=3): 27497(1.81%)
Total biological_process terms (lvl=3): 848576
Total unique biological_process terms (lvl=3): 433
Top 10 biological_process terms assigned (lvl=3): 
	1)GO:0043170-macromolecule metabolic process(L=3): 40318(4.75%)
	2)GO:0044260-cellular macromolecule metabolic process(L=3): 36182(4.26%)
	3)GO:1901360-organic cyclic compound metabolic process(L=3): 28091(3.31%)
	4)GO:0006725-cellular aromatic compound metabolic process(L=3): 27497(3.24%)
	5)GO:0034641-cellular nitrogen compound metabolic process(L=3): 26806(3.16%)
	6)GO:0046483-heterocycle metabolic process(L=3): 26634(3.14%)
	7)GO:1901576-organic substance biosynthetic process(L=3): 26589(3.13%)
	8)GO:0044249-cellular biosynthetic process(L=3): 26139(3.08%)
	9)GO:0019538-protein metabolic process(L=3): 24568(2.90%)
	10)GO:0006139-nucleobase-containing compound metabolic process(L=3): 24385(2.87%)
Total molecular_function terms (lvl=4): 277491
Total unique molecular_function terms (lvl=4): 561
Top 10 molecular_function terms assigned (lvl=4): 
	1)GO:0046872-metal ion binding(L=4): 24570(8.85%)
	2)GO:0017076-purine nucleotide binding(L=4): 22782(8.21%)
	3)GO:0032549-ribonucleoside binding(L=4): 22777(8.21%)
	4)GO:0032555-purine ribonucleotide binding(L=4): 22768(8.20%)
	5)GO:0001883-purine nucleoside binding(L=4): 22713(8.19%)
	6)GO:0035639-purine ribonucleoside triphosphate binding(L=4): 22260(8.02%)
	7)GO:0016301-kinase activity(L=4): 10063(3.63%)
	8)GO:0003723-RNA binding(L=4): 10034(3.62%)
	9)GO:0003677-DNA binding(L=4): 9953(3.59%)
	10)GO:0016818-hydrolase activity, acting on acid anhydrides, in phosphorus-containing anhydrides(L=4): 9794(3.53%)
Total cellular_component terms (lvl=4): 259725
Total unique cellular_component terms (lvl=4): 213
Top 10 cellular_component terms assigned (lvl=4): 
	1)GO:0043231-intracellular membrane-bounded organelle(L=4): 55404(21.33%)
	2)GO:0005737-cytoplasm(L=4): 54892(21.13%)
	3)GO:0044444-cytoplasmic part(L=4): 51410(19.79%)
	4)GO:0016021-integral component of membrane(L=4): 18272(7.04%)
	5)GO:0005829-cytosol(L=4): 17011(6.55%)
	6)GO:0005794-Golgi apparatus(L=4): 7434(2.86%)
	7)GO:0005618-cell wall(L=4): 6243(2.40%)
	8)GO:0030529-intracellular ribonucleoprotein complex(L=4): 5536(2.13%)
	9)GO:0009579-thylakoid(L=4): 5344(2.06%)
	10)GO:0005783-endoplasmic reticulum(L=4): 5178(1.99%)
Total overall terms (lvl=4): 1358317
Total unique overall terms (lvl=4): 2125
Top 10 overall terms assigned (lvl=4): 
	1)GO:0043231-intracellular membrane-bounded organelle(L=4): 55404(4.08%)
	2)GO:0005737-cytoplasm(L=4): 54892(4.04%)
	3)GO:0044444-cytoplasmic part(L=4): 51410(3.78%)
	4)GO:0046872-metal ion binding(L=4): 24570(1.81%)
	5)GO:0017076-purine nucleotide binding(L=4): 22782(1.68%)
	6)GO:0032549-ribonucleoside binding(L=4): 22777(1.68%)
	7)GO:0032555-purine ribonucleotide binding(L=4): 22768(1.68%)
	8)GO:0001883-purine nucleoside binding(L=4): 22713(1.67%)
	9)GO:0035639-purine ribonucleoside triphosphate binding(L=4): 22260(1.64%)
	10)GO:0006796-phosphate-containing compound metabolic process(L=4): 20679(1.52%)
Total biological_process terms (lvl=4): 821101
Total unique biological_process terms (lvl=4): 1351
Top 10 biological_process terms assigned (lvl=4): 
	1)GO:0006796-phosphate-containing compound metabolic process(L=4): 20679(2.52%)
	2)GO:0044267-cellular protein metabolic process(L=4): 20664(2.52%)
	3)GO:0090304-nucleic acid metabolic process(L=4): 17628(2.15%)
	4)GO:0009059-macromolecule biosynthetic process(L=4): 16810(2.05%)
	5)GO:0034645-cellular macromolecule biosynthetic process(L=4): 16643(2.03%)
	6)GO:0043412-macromolecule modification(L=4): 14126(1.72%)
	7)GO:0010467-gene expression(L=4): 13844(1.69%)
	8)GO:0036211-protein modification process(L=4): 13502(1.64%)
	9)GO:0031323-regulation of cellular metabolic process(L=4): 11736(1.43%)
	10)GO:0048731-system development(L=4): 11307(1.38%)
Total unique sequences with at least one pathway (KEGG) assignment: 35838
Total unique sequences without pathways (KEGG): 68874
Total pathways (KEGG) assigned: 130737
------------------------------------------------------
Final Annotation Statistics
------------------------------------------------------
Total Sequences: 130039
Similarity Search
	Total unique sequences with an alignment: 35793
	Total unique sequences without an alignment: 94246
Gene Families
	Total unique sequences with family assignment: 104712
	Total unique sequences without family assignment: 25327
	Total unique sequences with at least one GO term: 93868
	Total unique sequences with at least one pathway (KEGG) assignment: 35314
Totals
	Total unique sequences annotated (similarity search alignments only): 320
	Total unique sequences annotated (gene family assignment only): 69239
	Total unique sequences annotated (gene family and/or similarity search): 105032
	Total unique sequences unannotated (gene family and/or similarity search): 25007

EnTAP has completed! 
Total runtime (minutes): 2248
