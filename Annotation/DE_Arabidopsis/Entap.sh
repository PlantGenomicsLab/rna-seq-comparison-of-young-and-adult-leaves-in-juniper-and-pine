#!/bin/bash
#SBATCH --job-name=entap_DE
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 20
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date
##########################################
## EnTap				## 
##########################################
module load EnTAP/0.9.0-beta
module load diamond/0.9.19
module load GeneMarkS-T/5.1  

EnTAP --runP -i /labs/Wegrzyn/Juniper_Pine/DESeq2/Juniper/Juniper_DOWN.pep -d /labs/Wegrzyn/Juniper_Pine/araport_11.dmnd --contam bacteria --contam fungi --contam insecta --threads 20 --out-dir Juniper_DOWN

EnTAP --runP -i /labs/Wegrzyn/Juniper_Pine/DESeq2/Juniper/Juniper_UP.pep -d /labs/Wegrzyn/Juniper_Pine/araport_11.dmnd --contam bacteria --contam fungi --contam insecta --threads 20 --out-dir Juniper_UP

EnTAP --runP -i /labs/Wegrzyn/Juniper_Pine/DESeq2/Pine/Pine_DOWN.pep -d /labs/Wegrzyn/Juniper_Pine/araport_11.dmnd --contam bacteria --contam fungi --contam insecta --threads 20 --out-dir Pine_DOWN

EnTAP --runP -i /labs/Wegrzyn/Juniper_Pine/DESeq2/Pine/Pine_UP.pep -d /labs/Wegrzyn/Juniper_Pine/araport_11.dmnd --contam bacteria --contam fungi --contam insecta --threads 20 --out-dir Pine_UP











