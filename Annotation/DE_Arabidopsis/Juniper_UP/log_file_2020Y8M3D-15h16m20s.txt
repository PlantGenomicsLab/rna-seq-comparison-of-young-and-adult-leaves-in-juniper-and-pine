------------------------------------------------------
EnTAP Run Information - Execution
------------------------------------------------------
Current EnTAP Version: 0.9.0
Start time: Mon Aug  3 15:16:20 2020

Working directory has been set to: Juniper_UP_NEW

Execution Paths/Commands:

RSEM Directory: /isg/shared/apps/EnTAP/0.9.0-beta/libs/RSEM-1.3.0
GeneMarkS-T: perl /isg/shared/apps/EnTAP/0.9.0-beta/libs/gmst_linux_64/gmst.pl
DIAMOND: diamond
InterPro: interproscan.sh
EggNOG SQL Database: /isg/shared/apps/EnTAP/0.9.0-beta/databases/databases/eggnog.db
EggNOG DIAMOND Database: /isg/shared/apps/EnTAP/0.9.0-beta/databases/bin/eggnog_proteins.dmnd
EnTAP Database (binary): /isg/shared/apps/EnTAP/0.9.0-beta/databases/bin/entap_database.bin
EnTAP Database (SQL): /isg/shared/apps/EnTAP/0.9.0-beta//databases/entap_database.db
EnTAP Graphing Script: /isg/shared/apps/EnTAP/0.9.0-beta//src/entap_graphing.py

User Inputs:

contam: bacteria fungi insecta 
data-type: 0 
database: /labs/Wegrzyn/Juniper_Pine/araport_11.dmnd 
e: 1.00e-05
fpkm: 0.50
input: /labs/Wegrzyn/Juniper_Pine/VSEARCH/Juniper/Juniper_Up.pep
level: 0 3 4 
ontology: 0 
out-dir: Juniper_UP_NEW
output-format: 1 4 3 
protein: pfam 
qcoverage: 50.00
runP: null
state: +
tcoverage: 50.00
threads: 20

------------------------------------------------------
Transcriptome Statistics
------------------------------------------------------
Protein sequences found
Total sequences: 20
Total length of transcriptome(bp): 15543
Average sequence length(bp): 777.00
n50: 744
n90: 594
Longest sequence(bp): 1323 (JF1J_TRINITY_DN65099_c1_g1_i2.p1JF1J_TRINITY_DN65099_c1_g1~~JF1J_TRINITY_DN65099_c1_g1_i2.p1ORFtype:3prime_partiallen:382(+),score=28.01JF1J_TRINITY_DN65099_c1_g1_i2:293-1435(+))
Shortest sequence(bp): 579 (JF2J_TRINITY_DN27709_c1_g1_i1.p1JF2J_TRINITY_DN27709_c1_g1~~JF2J_TRINITY_DN27709_c1_g1_i1.p1ORFtype:internallen:128(+),score=10.54JF2J_TRINITY_DN27709_c1_g1_i1:2-382(+))
------------------------------------------------------
Similarity Search - DIAMOND - araport_11
------------------------------------------------------
Search results:
Juniper_UP_NEW/similarity_search/DIAMOND/blastp_Juniper_Up_final_araport_11.out
	Total alignments: 3
	Total unselected results: 1
		Written to: Juniper_UP_NEW/similarity_search/DIAMOND/processed//araport_11/unselected.tsv
	Total unique transcripts with an alignment: 2
		Reference transcriptome sequences with an alignment (FASTA):
			Juniper_UP_NEW/similarity_search/DIAMOND/processed//araport_11/best_hits_lvl0
		Search results (TSV):
			Juniper_UP_NEW/similarity_search/DIAMOND/processed//araport_11/best_hits_lvl0
	Total unique transcripts without an alignment: 18
		Reference transcriptome sequences without an alignment (FASTA):
			Juniper_UP_NEW/similarity_search/DIAMOND/processed//araport_11/no_hits.faa
	Total unique informative alignments: 2
	Total unique uninformative alignments: 0
	Total unique contaminants: 0(0.00%): 
		Transcriptome reference sequences labeled as a contaminant (FASTA):
			Juniper_UP_NEW/similarity_search/DIAMOND/processed//araport_11/best_hits_contam_lvl0
		Transcriptome reference sequences labeled as a contaminant (TSV):
			Juniper_UP_NEW/similarity_search/DIAMOND/processed//araport_11/best_hits_contam_lvl0
	Top 10 alignments by species:
			1): 2(100.00%)

------------------------------------------------------
Compiled Similarity Search - DIAMOND - Best Overall
------------------------------------------------------

	Total unique transcripts with an alignment: 2
		Reference transcriptome sequences with an alignment (FASTA):
			Juniper_UP_NEW/similarity_search/DIAMOND/overall_results/best_hits_lvl0
		Search results (TSV):
			Juniper_UP_NEW/similarity_search/DIAMOND/overall_results/best_hits_lvl0
	Total unique transcripts without an alignment: 18
		Reference transcriptome sequences without an alignment (FASTA):
			Juniper_UP_NEW/similarity_search/DIAMOND/overall_results/no_hits.faa
	Total unique informative alignments: 2
	Total unique uninformative alignments: 0
	Total unique contaminants: 0(0.00%): 
		Transcriptome reference sequences labeled as a contaminant (FASTA):
			Juniper_UP_NEW/similarity_search/DIAMOND/overall_results/best_hits_contam_lvl0
		Transcriptome reference sequences labeled as a contaminant (TSV):
			Juniper_UP_NEW/similarity_search/DIAMOND/overall_results/best_hits_contam_lvl0
	Top 10 alignments by species:
			1): 2(100.00%)

------------------------------------------------------
Gene Family - Gene Ontology and Pathway - EggNOG
------------------------------------------------------
Statistics for overall Eggnog results: 
Total unique sequences with family assignment: 17
Total unique sequences without family assignment: 3
Top 10 Taxonomic Scopes Assigned:
	1)Viridiplantae: 16(94.12%)
	2)Bacteria: 1(5.88%)
Total unique sequences with at least one GO term: 17
Total unique sequences without GO terms: 0
Total GO terms assigned: 1005
Total molecular_function terms (lvl=0): 180
Total unique molecular_function terms (lvl=0): 95
Top 10 molecular_function terms assigned (lvl=0): 
	1)GO:0003674-molecular_function(L=0): 13(7.22%)
	2)GO:0003824-catalytic activity(L=1): 10(5.56%)
	3)GO:0043167-ion binding(L=2): 7(3.89%)
	4)GO:0005488-binding(L=1): 7(3.89%)
	5)GO:1901363-heterocyclic compound binding(L=2): 4(2.22%)
	6)GO:0043169-cation binding(L=3): 4(2.22%)
	7)GO:0097159-organic cyclic compound binding(L=2): 4(2.22%)
	8)GO:0046872-metal ion binding(L=4): 4(2.22%)
	9)GO:0001883-purine nucleoside binding(L=4): 3(1.67%)
	10)GO:0017076-purine nucleotide binding(L=4): 3(1.67%)
Total cellular_component terms (lvl=0): 320
Total unique cellular_component terms (lvl=0): 109
Top 10 cellular_component terms assigned (lvl=0): 
	1)GO:0005575-cellular_component(L=0): 11(3.44%)
	2)GO:0044464-cell part(L=2): 11(3.44%)
	3)GO:0005623-cell(L=1): 11(3.44%)
	4)GO:0005622-intracellular(L=3): 10(3.12%)
	5)GO:0005737-cytoplasm(L=4): 10(3.12%)
	6)GO:0043226-organelle(L=1): 10(3.12%)
	7)GO:0044424-intracellular part(L=3): 10(3.12%)
	8)GO:0043229-intracellular organelle(L=3): 10(3.12%)
	9)GO:0044444-cytoplasmic part(L=4): 9(2.81%)
	10)GO:0043231-intracellular membrane-bounded organelle(L=4): 9(2.81%)
Total overall terms (lvl=0): 1005
Total unique overall terms (lvl=0): 493
Top 10 overall terms assigned (lvl=0): 
	1)GO:0008150-biological_process(L=0): 15(1.49%)
	2)GO:0003674-molecular_function(L=0): 13(1.29%)
	3)GO:0009987-cellular process(L=1): 12(1.19%)
	4)GO:0044464-cell part(L=2): 11(1.09%)
	5)GO:0008152-metabolic process(L=1): 11(1.09%)
	6)GO:0005623-cell(L=1): 11(1.09%)
	7)GO:0005575-cellular_component(L=0): 11(1.09%)
	8)GO:0043229-intracellular organelle(L=3): 10(1.00%)
	9)GO:0003824-catalytic activity(L=1): 10(1.00%)
	10)GO:0005622-intracellular(L=3): 10(1.00%)
Total biological_process terms (lvl=0): 505
Total unique biological_process terms (lvl=0): 289
Top 10 biological_process terms assigned (lvl=0): 
	1)GO:0008150-biological_process(L=0): 15(2.97%)
	2)GO:0009987-cellular process(L=1): 12(2.38%)
	3)GO:0008152-metabolic process(L=1): 11(2.18%)
	4)GO:0044237-cellular metabolic process(L=2): 8(1.58%)
	5)GO:0044699-single-organism process(L=1): 7(1.39%)
	6)GO:0044710-single-organism metabolic process(L=2): 7(1.39%)
	7)GO:0071704-organic substance metabolic process(L=2): 7(1.39%)
	8)GO:0044763-single-organism cellular process(L=2): 7(1.39%)
	9)GO:0044238-primary metabolic process(L=2): 6(1.19%)
	10)GO:0009058-biosynthetic process(L=2): 5(0.99%)
Total molecular_function terms (lvl=3): 33
Total unique molecular_function terms (lvl=3): 19
Top 10 molecular_function terms assigned (lvl=3): 
	1)GO:0043169-cation binding(L=3): 4(12.12%)
	2)GO:0000166-nucleotide binding(L=3): 3(9.09%)
	3)GO:0032553-ribonucleotide binding(L=3): 3(9.09%)
	4)GO:0001882-nucleoside binding(L=3): 3(9.09%)
	5)GO:1901265-nucleoside phosphate binding(L=3): 3(9.09%)
	6)GO:0043168-anion binding(L=3): 3(9.09%)
	7)GO:0016817-hydrolase activity, acting on acid anhydrides(L=3): 2(6.06%)
	8)GO:0016730-oxidoreductase activity, acting on iron-sulfur proteins as donors(L=3): 1(3.03%)
	9)GO:0009881-photoreceptor activity(L=3): 1(3.03%)
	10)GO:0080015-sabinene synthase activity(L=3): 1(3.03%)
Total cellular_component terms (lvl=3): 84
Total unique cellular_component terms (lvl=3): 21
Top 10 cellular_component terms assigned (lvl=3): 
	1)GO:0005622-intracellular(L=3): 10(11.90%)
	2)GO:0044424-intracellular part(L=3): 10(11.90%)
	3)GO:0043229-intracellular organelle(L=3): 10(11.90%)
	4)GO:0044446-intracellular organelle part(L=3): 8(9.52%)
	5)GO:0031224-intrinsic component of membrane(L=3): 6(7.14%)
	6)GO:0031090-organelle membrane(L=3): 6(7.14%)
	7)GO:0071944-cell periphery(L=3): 6(7.14%)
	8)GO:0005886-plasma membrane(L=3): 5(5.95%)
	9)GO:0030312-external encapsulating structure(L=3): 3(3.57%)
	10)GO:0031967-organelle envelope(L=3): 3(3.57%)
Total overall terms (lvl=3): 226
Total unique overall terms (lvl=3): 97
Top 10 overall terms assigned (lvl=3): 
	1)GO:0043229-intracellular organelle(L=3): 10(4.42%)
	2)GO:0005622-intracellular(L=3): 10(4.42%)
	3)GO:0044424-intracellular part(L=3): 10(4.42%)
	4)GO:0044446-intracellular organelle part(L=3): 8(3.54%)
	5)GO:0031090-organelle membrane(L=3): 6(2.65%)
	6)GO:0071944-cell periphery(L=3): 6(2.65%)
	7)GO:0031224-intrinsic component of membrane(L=3): 6(2.65%)
	8)GO:0005886-plasma membrane(L=3): 5(2.21%)
	9)GO:0006810-transport(L=3): 5(2.21%)
	10)GO:1901360-organic cyclic compound metabolic process(L=3): 4(1.77%)
Total biological_process terms (lvl=3): 109
Total unique biological_process terms (lvl=3): 57
Top 10 biological_process terms assigned (lvl=3): 
	1)GO:0006810-transport(L=3): 5(4.59%)
	2)GO:0044249-cellular biosynthetic process(L=3): 4(3.67%)
	3)GO:1901576-organic substance biosynthetic process(L=3): 4(3.67%)
	4)GO:0046483-heterocycle metabolic process(L=3): 4(3.67%)
	5)GO:1901360-organic cyclic compound metabolic process(L=3): 4(3.67%)
	6)GO:0034641-cellular nitrogen compound metabolic process(L=3): 4(3.67%)
	7)GO:0006725-cellular aromatic compound metabolic process(L=3): 4(3.67%)
	8)GO:0006139-nucleobase-containing compound metabolic process(L=3): 3(2.75%)
	9)GO:0043170-macromolecule metabolic process(L=3): 3(2.75%)
	10)GO:1901564-organonitrogen compound metabolic process(L=3): 3(2.75%)
Total molecular_function terms (lvl=4): 31
Total unique molecular_function terms (lvl=4): 17
Top 10 molecular_function terms assigned (lvl=4): 
	1)GO:0046872-metal ion binding(L=4): 4(12.90%)
	2)GO:0001883-purine nucleoside binding(L=4): 3(9.68%)
	3)GO:0017076-purine nucleotide binding(L=4): 3(9.68%)
	4)GO:0032555-purine ribonucleotide binding(L=4): 3(9.68%)
	5)GO:0032549-ribonucleoside binding(L=4): 3(9.68%)
	6)GO:0035639-purine ribonucleoside triphosphate binding(L=4): 3(9.68%)
	7)GO:0016818-hydrolase activity, acting on acid anhydrides, in phosphorus-containing anhydrides(L=4): 2(6.45%)
	8)GO:0020037-heme binding(L=4): 1(3.23%)
	9)GO:0003677-DNA binding(L=4): 1(3.23%)
	10)GO:0070011-peptidase activity, acting on L-amino acid peptides(L=4): 1(3.23%)
Total cellular_component terms (lvl=4): 53
Total unique cellular_component terms (lvl=4): 20
Top 10 cellular_component terms assigned (lvl=4): 
	1)GO:0005737-cytoplasm(L=4): 10(18.87%)
	2)GO:0044444-cytoplasmic part(L=4): 9(16.98%)
	3)GO:0043231-intracellular membrane-bounded organelle(L=4): 9(16.98%)
	4)GO:0016021-integral component of membrane(L=4): 5(9.43%)
	5)GO:0005618-cell wall(L=4): 3(5.66%)
	6)GO:0005794-Golgi apparatus(L=4): 2(3.77%)
	7)GO:0005829-cytosol(L=4): 2(3.77%)
	8)GO:0009579-thylakoid(L=4): 1(1.89%)
	9)GO:0005783-endoplasmic reticulum(L=4): 1(1.89%)
	10)GO:0030120-vesicle coat(L=4): 1(1.89%)
Total overall terms (lvl=4): 180
Total unique overall terms (lvl=4): 110
Top 10 overall terms assigned (lvl=4): 
	1)GO:0005737-cytoplasm(L=4): 10(5.56%)
	2)GO:0044444-cytoplasmic part(L=4): 9(5.00%)
	3)GO:0043231-intracellular membrane-bounded organelle(L=4): 9(5.00%)
	4)GO:0016021-integral component of membrane(L=4): 5(2.78%)
	5)GO:0046872-metal ion binding(L=4): 4(2.22%)
	6)GO:0035639-purine ribonucleoside triphosphate binding(L=4): 3(1.67%)
	7)GO:0017076-purine nucleotide binding(L=4): 3(1.67%)
	8)GO:0032555-purine ribonucleotide binding(L=4): 3(1.67%)
	9)GO:0032549-ribonucleoside binding(L=4): 3(1.67%)
	10)GO:0005618-cell wall(L=4): 3(1.67%)
Total biological_process terms (lvl=4): 96
Total unique biological_process terms (lvl=4): 73
Top 10 biological_process terms assigned (lvl=4): 
	1)GO:0071702-organic substance transport(L=4): 3(3.12%)
	2)GO:0044267-cellular protein metabolic process(L=4): 2(2.08%)
	3)GO:1901658-glycosyl compound catabolic process(L=4): 2(2.08%)
	4)GO:0019439-aromatic compound catabolic process(L=4): 2(2.08%)
	5)GO:0006753-nucleoside phosphate metabolic process(L=4): 2(2.08%)
	6)GO:0046434-organophosphate catabolic process(L=4): 2(2.08%)
	7)GO:0006796-phosphate-containing compound metabolic process(L=4): 2(2.08%)
	8)GO:0019693-ribose phosphate metabolic process(L=4): 2(2.08%)
	9)GO:0015031-protein transport(L=4): 2(2.08%)
	10)GO:0046700-heterocycle catabolic process(L=4): 2(2.08%)
Total unique sequences with at least one pathway (KEGG) assignment: 9
Total unique sequences without pathways (KEGG): 8
Total pathways (KEGG) assigned: 30
------------------------------------------------------
Final Annotation Statistics
------------------------------------------------------
Total Sequences: 20
Similarity Search
	Total unique sequences with an alignment: 2
	Total unique sequences without an alignment: 18
Gene Families
	Total unique sequences with family assignment: 17
	Total unique sequences without family assignment: 3
	Total unique sequences with at least one GO term: 15
	Total unique sequences with at least one pathway (KEGG) assignment: 9
Totals
	Total unique sequences annotated (similarity search alignments only): 0
	Total unique sequences annotated (gene family assignment only): 15
	Total unique sequences annotated (gene family and/or similarity search): 17
	Total unique sequences unannotated (gene family and/or similarity search): 3

EnTAP has completed! 
Total runtime (minutes): 1
