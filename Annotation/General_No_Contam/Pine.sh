#!/bin/bash
#SBATCH --job-name=entap
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 30
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date
module load EnTAP/0.9.0-beta
module load diamond/0.9.19
module load GeneMarkS-T/5.1   


EnTAP --runP -i /labs/Wegrzyn/Juniper_Pine/Clustering/Pine.pep -d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.87.dmnd  -d /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.97.dmnd -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd --taxon pinaceae --contam bacteria --contam fungi --contam insecta  --threads 30 --out-dir Pine
