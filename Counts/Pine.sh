#!/bin/bash
#SBATCH --job-name=kallisto_counts_pine
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname

module load kallisto/0.44.0


kallisto quant -i ../Index/pine_index \
        -o PC1A \
        -t 8 --pseudobam \
        /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_PC1A_index2_R1_trimmed.fastq /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_PC1A_index2_R2_trimmed.fastq

kallisto quant -i ../Index/pine_index \
        -o PC2A \
        -t 8 --pseudobam \
        /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_PC2A_index4_R1_trimmed.fastq /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_PC2A_index4_R2_trimmed.fastq

kallisto quant -i ../Index/pine_index \
        -o PC3A \
        -t 8 --pseudobam \
        /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_PC3A_index6_R1_trimmed.fastq /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_PC3A_index6_R2_trimmed.fastq

kallisto quant -i ../Index/pine_index \
        -o PC1J \
        -t 8 --pseudobam \
	     /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_PC1J_index12_R1_trimmed.fastq /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_PC1J_index12_R2_trimmed.fastq

kallisto quant -i ../Index/pine_index \
        -o PC2J \
        -t 8 --pseudobam \
	     /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_PC2J_index35_R1_trimmed.fastq /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_PC2J_index35_R2_trimmed.fastq

kallisto quant -i ../Index/pine_index \
        -o PC3J \
        -t 8 --pseudobam \
	     /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_PC3J_index7_R1_trimmed.fastq /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_PC3J_index7_R2_trimmed.fastq


