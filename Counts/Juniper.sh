#!/bin/bash
#SBATCH --job-name=kallisto_counts_juniper
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname

module load kallisto/0.44.0

kallisto quant -i ../Index/juniper_index \
        -o JF1A \
        -t 8 --pseudobam \
    		/labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_JF1A_index6_R1_trimmed.fastq /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_JF1A_index6_R2_trimmed.fastq


kallisto quant -i ../Index/juniper_index \
        -o JF2A \
        -t 8 --pseudobam \
        /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_JF2A_index7_R1_trimmed.fastq /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_JF2A_index7_R2_trimmed.fastq

kallisto quant -i ../Index/juniper_index \
        -o JF3A \
        -t 8 --pseudobam \
        /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_JF3A_index12_R1_trimmed.fastq /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_JF3A_index12_R2_trimmed.fastq

kallisto quant -i ../Index/juniper_index \
        -o JF1J \
        -t 8 --pseudobam \
        /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_JF1J_index4_R1_trimmed.fastq /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_JF1J_index4_R2_trimmed.fastq

kallisto quant -i ../Index/juniper_index \
        -o JF2J \
        -t 8 --pseudobam \
        /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_JF2J_index5_R1_trimmed.fastq /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_JF2J_index5_R2_trimmed.fastq

kallisto quant -i ../Index/juniper_index \
        -o JF3J \
        -t 8 --pseudobam \
        /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_JF3J_index2_R1_trimmed.fastq /labs/Wegrzyn/Juniper_Pine/Quality_Control/all_JPJ_JF3J_index2_R2_trimmed.fastq


