#!/bin/bash
#SBATCH --job-name=kallisto_index
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=20G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
##SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "kallisto_index"

module load kallisto/0.44.0

kallisto index -i juniper_index /labs/Wegrzyn/Juniper_Pine/Clustering/Juniper.fasta

kallisto index -i pine_index /labs/Wegrzyn/Juniper_Pine/Clustering/Pine.fasta


