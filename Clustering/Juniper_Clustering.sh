#!/bin/bash
#SBATCH --job-name=vsearch_Juniper
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=100G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "vsearch_pine"

module load vsearch/2.4.3

vsearch --threads 8 --log LOGFile \
        --cluster_fast /labs/Wegrzyn/Juniper_Pine/Clustering/Contam_Removal_Juniper/Juniper_1/Juniper_300.cds \
        --id 0.90 \
        --centroids centroids_Juniper.fasta \
        --uc clusters_Juniper.uc 
